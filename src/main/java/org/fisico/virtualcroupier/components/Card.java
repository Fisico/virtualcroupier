/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fisico.virtualcroupier.components;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.mongodb.morphia.annotations.Embedded;

import java.util.Comparator;
import java.util.Objects;

@Embedded
public class Card implements Comparator<Card> {

    private final static String DEFAULT_FORE_COLOR = "#000000";
    private final static String DEFAULT_BACK_COLOR = "#FFFFFF";

    private String name;
    private String foreColor;
    private String backColor;
    private boolean tapped;

    // Morphia needs it
    @SuppressWarnings("unused")
    private Card() {
    	super();
    }
    
    public Card(String name, String foreColor, String backColor, boolean tapped) {
        if(name == null) {
            throw new IllegalArgumentException("The name of a card can not be empty");
        }
        this.name = name;
        this.foreColor = foreColor;
        this.backColor = backColor;
        this.tapped = tapped;
    }

    public Card(String name, String foreColor, String backColor) {
        this(name, foreColor, backColor, false);
    }

    public Card(String name, String foreColor) {
        this(name, foreColor, DEFAULT_BACK_COLOR, false);
    }

    public Card(String name) {
        this(name, DEFAULT_FORE_COLOR, DEFAULT_BACK_COLOR, false);
    }
    
    public Card(Card other)
    {
        this.name = other.name;
        this.foreColor = other.foreColor;
        this.backColor = other.backColor;
        this.tapped = other.tapped;
    }

    public String toString() {
    	StringBuilder builder = new StringBuilder();
    	if( !backColor.equalsIgnoreCase(DEFAULT_BACK_COLOR) ) {
    		builder.append("[BGCOLOR=").append(backColor).append("]");
    	}
    	if( !foreColor.equalsIgnoreCase(DEFAULT_FORE_COLOR))
    	{
    		builder.append("[COLOR=").append(foreColor).append("]");
    	}
    	if( tapped ) {
    		builder.append("*");
    	}
    	builder.append(name);
    	if( tapped ) {
    		builder.append("*");
    	}
    	if( !foreColor.equalsIgnoreCase(DEFAULT_FORE_COLOR))
    	{
    		builder.append("[/COLOR]");
    	}
    	if( !backColor.equalsIgnoreCase(DEFAULT_BACK_COLOR) ) {
    		builder.append("[/BGCOLOR]");
    	}	
    	
        return builder.toString();
    }

	public boolean isTapped() {
		return tapped;
	}

	public void setTapped(boolean tapped) {
		this.tapped = tapped;
	}

    public boolean isSameNameAs(String cardName) {
        if(name.equalsIgnoreCase(cardName)) {
            return true;
        }
        String modifiedCard = "[" + cardName + "]";
        if(name.equalsIgnoreCase(modifiedCard)) {
            return true;
        }
        modifiedCard = cardName.replace(' ', '_');
        if(name.equalsIgnoreCase(modifiedCard)) {
            return true;
        }
        return false;
    }

    @Override
    public int compare(Card o1, Card o2) {
        return o1.name.compareToIgnoreCase(o2.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Card card = (Card) o;

        return isSameValueAs(card);
    }

    public boolean isSameValueAs(Card card) {
        return new EqualsBuilder()
                .append(tapped, card.tapped)
                .append(name, card.name)
                .append(foreColor, card.foreColor)
                .append(backColor, card.backColor)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(foreColor)
                .append(backColor)
                .append(tapped)
                .toHashCode();
    }
}
