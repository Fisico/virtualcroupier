/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fisico.virtualcroupier.components;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class Deck  {
    public static final int NOT_FOUND = -2;

	protected String name;
    // The card list is in raw order, result of inserts and removes
    // This order is purely internal
	protected List<Card> cards;
    // The relative position of the cards inside the deck
    // The first integer is the position of the card in the deck
    // The second integer is the position of the card in the internal list
    // The top card is the number 1, the number 2 the second, etc.
	private Map<Integer,Integer> order;
    // The deck representation in the outside is by this order
    // The first integer is the order in what the cards are listed
    // The second integer is the position of the card in the internal list
    // The first card shown is the number 1, the number 2 the second, etc.
	private Map<Integer,Integer> canonical;
	protected boolean visibleWhenEmpty;

	// Morphia needs it
	@SuppressWarnings("unused")
	private Deck() {
		super();
	}

	public Deck(String name) {
		this.name = name;
		cards = new ArrayList<Card>();
		visibleWhenEmpty = true;
		order = new HashMap<Integer,Integer>();
		canonical = new HashMap<Integer,Integer>();
	}

	public Deck(String name, List<Card> cards) {
		this.name = name;
		this.cards = new ArrayList<Card>();
        visibleWhenEmpty = true;
        order = new HashMap<Integer,Integer>();
        canonical = new HashMap<Integer,Integer>();
		for (Card c : cards) {
            put(DeckOrderPosition.BOTTOM, c);
        }
	}

	/**
	 * Gets the card in a given position but without taking it out of the deck
	 *
	 * @param deckOrderPosition
	 * @return The card in the position
	 */
	public Card peek(DeckOrderPosition deckOrderPosition) {
		int listPosition = getListPosition(deckOrderPosition);

		return cards.get(listPosition);
	}

	/**
	 * Blind draw. Extract a card by position in deck order, not canonical order.
	 * 
	 *
	 * @param deckOrderPosition@return The card in that position
	 */
	public Card draw(DeckOrderPosition deckOrderPosition) {
		int listPosition = getListPosition(deckOrderPosition);
		Card card = cards.get(listPosition);
		removeCard(listPosition);

		return card;
	}

	/**
	 * Blind draw. Extract the top card by position in deck order.
	 * 
	 * @return The card in the top position
	 */
	public Card draw() {
        return draw(DeckOrderPosition.TOP);
	}

	/**
	 * Put a card in the position in deck order indicated. The card is added in alphabetical order to the canonical position.
	 *
	 * @param deckOrderPosition
	 * @param card The card to add to the deck
	 */
	public void put(DeckOrderPosition deckOrderPosition, Card card) {
		int correctedPos = getInsertPosition(deckOrderPosition.value());
		int canonicalPos = getAlphabeticalPos(card).value();
		int listPosition = cards.size();
		canonical = addItem(canonical, canonicalPos, listPosition);
		order = addItem(order, correctedPos, listPosition);
		cards.add(card);
	}

    /**
	 * Shuffle the deck three times and cut the deck.
	 */
	public void shuffle() {
		if (!cards.isEmpty()) {
			shuffle(3);
			cut();
		}
	}

	/**
	 * Shuffle the deck a fixed number of times 
	 * 
	 * @param times How many times to shuffle the deck
	 */
	public void shuffle(int times) {
		for (int i = 0; i < times; i++) {
			Random rand = new Random();
			rand.setSeed(Date.from(Instant.now()).getTime());
			for( int j = 0 ; j < cards.size() ; j++ )
			{
				int pivot1 = rand.nextInt(cards.size()) + 1;
				int pivot2 = rand.nextInt(cards.size()) + 1;
				if(pivot1 != pivot2)
				{
					int aux = order.get(pivot1);
					order.put(pivot1, order.get(pivot2));
					order.put(pivot2, aux);
				}
			}
		}	
	}

	/**
	 * Cut the deck by a random position
	 */
	public void cut() {
		Random rand = new Random();
		rand.setSeed(Date.from(Instant.now()).getTime());
		int start = rand.nextInt(cards.size() - 1);
		order = calculatePositionsAfterCut(start + 1);
	}

	/**
	 * Check if the deck is empty
	 * 
	 * @return True if the deck has no cards and false otherwise
	 */
	public boolean isEmpty() {
		return cards.isEmpty();
	}

	/**
	 * Get the name of the deck
	 * 
	 * @return The name of the deck
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Get the size of the deck
	 * 
	 * @return The number of cards in the deck
	 */
	public int size() {
		return cards.size();
	}

	/**
	 * Search the deck for a given card 
	 * 
	 * @param card The name of the card to locate
	 * @return The position of the card in deck order.
	 */
	public DeckOrderPosition locate(String card) {
		int orderPosition = NOT_FOUND;
		int listPosition = NOT_FOUND;
		for (int i = 0; i < cards.size() && listPosition == NOT_FOUND; i++) {
			if (cards.get(i).isSameNameAs(card)) {
				listPosition = i;
			}
		}
		if(listPosition != NOT_FOUND)
		{
			orderPosition = getEntry(order, listPosition);
		}

		return new DeckOrderPosition(orderPosition);
	}

	private boolean isSameName(String name, String card) {
		if(name.equalsIgnoreCase(card)) {
            return true;
        }
        String modifiedCard = "[" + card + "]";
        if(name.equalsIgnoreCase(modifiedCard)) {
            return true;
        }
        modifiedCard = card.replace(' ', '_');
        if(name.equalsIgnoreCase(modifiedCard)) {
            return true;
        }
        return false;
	}

	/**
	 * 
	 *
     * @param canonicalPosition
     * @return The position of the card in deck order
	 */
	public DeckOrderPosition locate(CanonicalPosition canonicalPosition)
	{
		int orderPosition = NOT_FOUND;
		if(isExistingPosition(canonicalPosition.value())) {
			int listPosition = canonical.get(canonicalPosition.value());
			orderPosition = getEntry(order, listPosition);
		} 
		
		return new DeckOrderPosition(orderPosition);
	}

	/**
	 * Sets if this deck is shown when it is empty
	 * 
	 * @param visible True to show it and false to not show it
	 */
	public void setVisibleWhenEmpty(boolean visible) {
		visibleWhenEmpty = visible;
	}

	/**
	 * Converts the deck to a String representing the cards in the deck. The cards are ordered in the canonical position.
	 */
	public String toString()
	{
		StringBuilder deck = new StringBuilder();
		if (visibleWhenEmpty || !isEmpty()) {
			deck.append("[q=\"").append(name).append("\"]\n");
			if(isMonoCard()) {
                Card c = cards.get(0);
                deck.append(c.toString()).append("x").append(cards.size()).append("\n");
            } else {
                for (Integer entry : canonical.keySet()) {
                    int listPosition = canonical.get(entry);
                    Card c = cards.get(listPosition);
                    deck.append(entry).append(" - ").append(c.toString()).append("\n");
                }
            }
			deck.append("[/q]\n");
		}

		return deck.toString();
	}

    private boolean isMonoCard() {
        boolean mono = false;
        if(size()>1) {
            mono = true;
            Card example = cards.get(0);
            for(Card c : cards) {
                if(!c.toString().equalsIgnoreCase(example.toString())) {
                    mono = false;
                }
            }
        }

        return mono;
    }

    private int correctPosition(int position)
	{
		int orderPosition = position;
        if (position == DeckOrderPosition.BOTTOM.getPosition()) {
			orderPosition = cards.size();
		}

		return orderPosition;
	}
	
	private int getInsertPosition(int orderPosition) {
		int correctedPos = orderPosition;
        if(orderPosition == DeckOrderPosition.BOTTOM.getPosition()) {
			correctedPos = cards.size() + 1;
		}
		if(!isValidPosition(correctedPos)){
			throw new IllegalArgumentException( orderPosition + " is not a valid position");
		}
		return correctedPos;
	}

    // Gets the position in the internal list of a card from the position in the deck
	private int getListPosition(DeckOrderPosition deckOrderPosition) throws IllegalArgumentException{
        int correctedPosition = correctPosition(deckOrderPosition.value());
		if(!isExistingPosition(correctedPosition)) {
			throw new IllegalArgumentException("Card does not exist");
		}
		int listPosition = order.get(correctedPosition);

		return listPosition;
	}

	private Map<Integer,Integer> calculatePositionsAfterCut(int start) {
		Map<Integer,Integer> remap = new HashMap<Integer,Integer>();
		for(int i = 1 ; i <= order.size(); i++ )
		{
			remap.put(i, calculateTarget(start, i));
		}

		return remap;
	}

	private int calculateTarget( int start, int canonical ) {
		int before = start + canonical;
		if(before > order.size()) {
			before = before - order.size();
		}

		return order.get(before);
	}

	private void removeCard(int listPosition) {
		int canonicalPos = getEntry(canonical, listPosition);
		canonical = removeItem(canonical, canonicalPos, listPosition);
		int orderPos = getEntry(order, listPosition);
		order = removeItem(order, orderPos, listPosition);
		cards.remove(listPosition);
	}

	private Map<Integer, Integer> addItem(Map<Integer,Integer> map, int key, int value) {
		HashMap<Integer, Integer> remap = new HashMap<Integer,Integer>();
		for(int entry = 1; entry <= map.size(); entry++){
			int pointsTo = map.get(entry);
			int nowPointsTo = pointsTo;
			if(pointsTo >= value) {
				nowPointsTo++;
			}
			if( entry < key ) {
				remap.put(entry, nowPointsTo);
			} else if( entry >= key) {
				remap.put(entry + 1, nowPointsTo);
			}
		}
		remap.put(key, value);

		return remap;
	}

	private Map<Integer, Integer> removeItem(Map<Integer,Integer> map, int key, int value) {
		HashMap<Integer, Integer> remap = new HashMap<Integer,Integer>();
		for(int entry = 1; entry <= map.size(); entry++){
			int pointsTo = map.get(entry);
			int nowPointsTo = pointsTo;
			if(pointsTo > value) {
				nowPointsTo--;
			}
			if( entry < key ) {
				remap.put(entry, nowPointsTo);
			} else if( entry > key) {
				remap.put(entry - 1, nowPointsTo);
			}
		}

		return remap;
	}

	private int getEntry(Map<Integer,Integer> map, int value) {
		for(int entry = 1; entry <= map.size(); entry++){
			if(map.get(entry) == value) {
				return entry;
			}
		}

		return NOT_FOUND;
	}

	private boolean isValidPosition(int position) {
		boolean isValid = true;
		if (position < 1 ) {
			isValid = false;
		} else if(position > cards.size() + 1) {
			isValid = false;
		}

		return isValid;
	}

	private boolean isExistingPosition(int position) {
		boolean isValid = true;
		if (position < 1 ) {
			isValid = false;
		} else if(position > cards.size()) {
			isValid = false;
		}

		return isValid;
	}

	private CanonicalPosition getAlphabeticalPos(Card card) {
        int min = 0;
        int max = Integer.MAX_VALUE;
        for(Integer pos: canonical.keySet()) {
            Card c = cards.get(canonical.get(pos));
            int relativePos = card.compare(card, c);
            if( relativePos > 0 && pos > min ) {
                min = pos;
            } else if( relativePos < 0 && pos < max) {
                max = pos;
            }
        }
        if(min == 0) {
            return new CanonicalPosition(1);
        } else if(max == Integer.MAX_VALUE) {
            return new CanonicalPosition(canonical.size() + 1);
        } else {
            return new CanonicalPosition(max);
        }
    }
}
