package org.fisico.virtualcroupier.components;

public class DeckOrderPosition {
    public static final DeckOrderPosition TOP = new DeckOrderPosition(1);
    public static final DeckOrderPosition BOTTOM = new DeckOrderPosition(-1);

    public int getPosition() {
        return position;
    }

    private final int position;

    /**
     * @param position The position of the card in deck order
     */
    public DeckOrderPosition(int position) {
        this.position = position;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final DeckOrderPosition that = (DeckOrderPosition) o;

        return sameValueAs(that);
    }

    @Override
    public int hashCode() {
        return position;
    }

    public boolean sameValueAs(DeckOrderPosition other) {
        return other != null && other.position == this.position;
    }

    public int value() {
        return position;
    }

}
