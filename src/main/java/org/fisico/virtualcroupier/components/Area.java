package org.fisico.virtualcroupier.components;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class Area {
	protected String name;
	protected String owner;
	protected Visibility visibility;
	private List<Area> sections;
	private List<Deck> decks;
	protected List<Card> cards;
	protected boolean visibleWhenEmpty;

	// Morphia needs it
	@SuppressWarnings("unused")
	private Area() {
		super();
		this.sections = new ArrayList<Area>();
		this.decks = new ArrayList<Deck>();
		this.cards = new ArrayList<Card>();
	}

	public Area(String name, String owner) {
		super();
		this.name = name;
		this.owner = owner;
		this.visibility = Visibility.PUBLIC_AREA;
		this.sections = new ArrayList<Area>();
		this.decks = new ArrayList<Deck>();
		this.cards = new ArrayList<Card>();
	}

	public Area(String name, String owner, Visibility visibility) {
		super();
		this.name = name;
		this.owner = owner;
		this.visibility = visibility;
		this.sections = new ArrayList<Area>();
		this.decks = new ArrayList<Deck>();
		this.cards = new ArrayList<Card>();
	}

	public String getName() {
		return name;
	}

	public boolean isOwner(String player) {
        return owner.equalsIgnoreCase(player);
	}

	public String toString() {
		StringBuilder area = new StringBuilder();
		area.append("[q=\"").append(name).append("\"]\n");
		for( int i = 0; i < cards.size() ; i++)
		{
			Card c = cards.get(i);
			area.append(i).append(" - ").append(c.toString()).append("\n");
		}
		for(Object d : decks )
		{
			area.append(d.toString());
			area.append("\n");
		}
		for (Area a : sections) {
			area.append(a.toString());
			area.append("\n");
		}
		area.append("[/q]");
		area.append("\n");

		return area.toString();
	}

	public void addArea(Area container) {
        sections.add(container);
	}

	public void addDeck(Deck deck){
		decks.add(deck);
	}

	public void addCard(Card card) {
		cards.add(card);
	}

	public Deck selectDeck(String deckName, String pov) {
		Deck selected = null;
		if( hasAccess(pov) ) {
			selected = searchDecks(deckName);
			if(selected == null)
			{
				selected = searchAreasOwnedForDeck(deckName, pov);
				if(selected == null) {
					selected = searchAllAreasForDeck(deckName, pov);
				}
			}
		}

		return selected;
	}

    public Area selectArea(String areaName, String pov) {
        Area selected = null;
        if( hasAccess(pov) ) {
            if(areaName.equalsIgnoreCase(name)) {
                selected = this;
            } else {
                selected = searchAreasOwnedForArea(areaName, pov);
                if(selected == null) {
                    selected = searchAllAreasForArea(areaName, pov);
                }
            }
        }

        return selected;
    }

	public void removeArea(String areaName) {
		Area toRemove = null;
		for(Area area: sections)
		{
			area.removeArea(areaName);
			if(area.name.equalsIgnoreCase(areaName)){
				toRemove = area;
				break;
			}
		}
		if(toRemove != null)
		{
			sections.remove(toRemove);
		} 

	}

	public void removeDeck(String deckName) {
		Object toRemove = null;	
		toRemove = searchDecks(deckName);
		if(toRemove != null) {
			decks.remove(toRemove);
		}
		for(Area area: sections)
		{
			area.removeDeck(deckName);
		}
	}

	public void removeCard(String cardName)
	{
		Card toRemove = null;
		for(Card card : cards)
		{
			if( card.isSameNameAs(cardName) )
			{
				toRemove = card;
				break;
			}
		}

		cards.remove(toRemove);
	}

	public void setVisibleWhenEmpty(boolean visible) {
		visibleWhenEmpty = visible;
	}

	public String seenAs(String pov, Visibility visibility) {
		StringBuilder area = new StringBuilder();
		if( isVisible(pov) && this.visibility == visibility) {
			area.append("[q=\"").append(name).append("\"]\n");
			for( int i = 0; i < cards.size() ; i++)
			{
				Card c = cards.get(i);
				area.append(i).append(" - ").append(c.toString()).append("\n");
			}
			for(Deck d : decks )
			{
				area.append(d.toString());
				area.append("\n");
			}
			for (Area a : sections) {
				area.append(a.seenAs(pov, visibility));
				area.append("\n");
			}
			area.append("[/q]");
			area.append("\n");
		}

		return area.toString();
	}

	public boolean isEmpty() {
		boolean empty = true;
		if(cards.size() > 0) {
			empty = false;
		}
		for(Deck deck: decks) {
			if(!deck.isEmpty()) {
				empty = false;
			}
		}
		for(Area area: sections) {
			if(!area.isEmpty()) {
				empty = false;
			}
		}
		
		return empty;
	}

	public boolean hasAccess(String pov) {
		boolean visible = false;
		if(visibility == Visibility.PUBLIC_AREA) {
			visible = true;
		}
		if(owner.equalsIgnoreCase(pov)) {
			visible = true;
		}

		return visible;
	}

	public boolean isVisible(String pov) {
		boolean visible = true;
		if(isEmpty() && !visibleWhenEmpty) {
			visible = false;
		} else if(!hasAccess(pov)) {
			visible = false;
		}
		
		return visible;
	}

	public void changeVisibility(Visibility visibility) {
        this.visibility = visibility;
	}
	
	private Deck searchDecks(String deckName) {
		Deck selected = null;
		for(Deck deck : decks)
		{
			if(deck.name.equalsIgnoreCase(deckName))
			{
				selected = deck;
				break;
			}
		}
		
		return selected;
	}
	
	private Deck searchAreasOwnedForDeck(String deckName, String pov) {
		Deck selected = null;
		for(Area container: sections)
		{
			if(container.isOwner(pov)) {
				selected = container.selectDeck(deckName, pov);
				if( selected != null )
				{
					break;
				}
			}
		}
		
		return selected;
	}
	
	private Deck searchAllAreasForDeck(String deckName, String pov) {
		Deck selected = null;
		for(Area container: sections)
		{
			selected = container.selectDeck(deckName, pov);
			if( selected != null )
			{
				break;
			}
		}
		
		return selected;
	}
    private Area searchAreasOwnedForArea(String areaName, String pov) {
        Area selected = null;
        for(Area container: sections)
        {
            if(container.isOwner(pov)) {
                selected = container.selectArea(areaName, pov);
                if( selected != null )
                {
                    break;
                }
            }
        }

        return selected;
    }

    private Area searchAllAreasForArea(String areaName, String pov) {
        Area selected = null;
        for(Area container: sections)
        {
            selected = container.selectArea(areaName, pov);
            if( selected != null )
            {
                break;
            }
        }

        return selected;
    }
}
