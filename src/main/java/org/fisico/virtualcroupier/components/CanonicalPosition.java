package org.fisico.virtualcroupier.components;

public class CanonicalPosition {
    private final int position;

    /**
     * @param position The canonical position of the card to locate
     */
    public CanonicalPosition(int position) {
        this.position = position;
    }

    public int value() {
        return position;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final CanonicalPosition that = (CanonicalPosition) o;

        return sameValueAs(that);
    }

    @Override
    public int hashCode() {
        return position;
    }

    public boolean sameValueAs(CanonicalPosition other) {
        return other != null && other.position == this.position;
    }

}
