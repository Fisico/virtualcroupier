package org.fisico.virtualcroupier.components;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

public class TableState {

	private String stateId;
	private List<Area> areas;

	public TableState() {
		areas = new ArrayList<Area>();
	}

	public TableState(String stateId, List<Area> areas) {
		this.stateId = stateId;
		this.areas = areas;
	}

	public String getId() {
		return stateId;
	}

	public void setId(String stateId) {
        this.stateId = stateId;
    }

	public List<Area> getAreas() {
		return areas;
	}

	public void addArea(Area area) {
		areas.add(area);
	}

	public Deck locateDeck(String deckName, String pov) {
		Deck selected = null;
		for(Area area: areas)
		{
			if(area.isOwner(pov)) {
				selected = area.selectDeck(deckName, pov);
				if( selected != null ) {
					break;
				}
			}
		}
		if(selected == null) {
			for(Area area: areas)
			{
				selected = area.selectDeck(deckName, pov);
				if( selected != null ) {
					break;
				}
			}
		}

		return selected;
	}

	public Area locateArea(String areaName, String pov) {
		Area  selected = null;
		for(Area area: areas)
		{
			if(area.isOwner(pov)) {
                selected = area.selectArea(areaName, pov);
				if( selected != null ) {
					break;
				}
			}
		}
		if(selected == null) {
			for(Area area: areas)
			{
                selected = area.selectArea(areaName, pov);
                if( selected != null ) {
					break;
				}
			}
		}

        return selected;
	}


	public String seenAs(String user, Visibility visibility) {
		StringBuilder state = new StringBuilder();
		for (Area area : areas) {
			state.append(area.seenAs(user, visibility));
			state.append("\n");
		}
		state.append("\n");

		return state.toString();
	}
}
