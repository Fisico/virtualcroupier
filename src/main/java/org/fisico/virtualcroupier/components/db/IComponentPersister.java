package org.fisico.virtualcroupier.components.db;

import org.fisico.virtualcroupier.components.TableState;

public interface IComponentPersister {
	void saveTableState(TableState state);
	TableState loadTableState(String id);
}
