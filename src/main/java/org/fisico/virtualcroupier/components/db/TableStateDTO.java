package org.fisico.virtualcroupier.components.db;

import org.bson.types.ObjectId;
import org.fisico.virtualcroupier.components.Area;
import org.fisico.virtualcroupier.components.TableState;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.dao.BasicDAO;

import java.util.List;

@Entity("tablestate")
public class TableStateDTO {

    @Id
    private ObjectId stateId;
    private List<Area> areas;

    public TableStateDTO() {

    }

    public TableStateDTO(TableState state) {
        if(state.getId() == null) {
            this.stateId = new ObjectId();
        } else {
            this.stateId = new ObjectId(state.getId());
        }
        this.areas = state.getAreas();
    }

    public ObjectId getStateId() {
        return stateId;
    }

    public void setStateId(ObjectId stateId) {
        this.stateId = stateId;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public TableState toTableState() {
        return new TableState(stateId.toHexString(), areas);
    }
}
