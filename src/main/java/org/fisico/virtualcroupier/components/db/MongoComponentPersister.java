package org.fisico.virtualcroupier.components.db;

import java.util.List;

import org.bson.types.ObjectId;
import org.fisico.virtualcroupier.components.TableState;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import com.mongodb.MongoClient;
import org.springframework.stereotype.Repository;

@Repository
public class MongoComponentPersister implements IComponentPersister {

	private Datastore datastore;

	public MongoComponentPersister() {
		Morphia morphia = new Morphia();
		morphia.mapPackage("org.fisico.virtualcroupier.components");
		morphia.getMapper().getOptions().setStoreEmpties(true);
		morphia.getMapper().getOptions().setStoreNulls(true);
		datastore = morphia.createDatastore(new MongoClient(), "virtualcroupier");
	}

	@Override
	public void saveTableState(TableState state) {
        TableStateDTO dto = new TableStateDTO(state);
		datastore.save(dto);
        state.setId(dto.getStateId().toHexString());
	}

	@Override
	public TableState loadTableState(String id) {

        ObjectId objectId = new ObjectId(id);
        TableStateDTO dto = datastore.get(TableStateDTO.class, objectId);

		return dto.toTableState();
	}
}
