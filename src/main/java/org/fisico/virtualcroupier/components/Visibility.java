package org.fisico.virtualcroupier.components;


public enum Visibility {

    PUBLIC_AREA, TEAM_AREA, PRIVATE_AREA
};
