package org.fisico.virtualcroupier.game.base;

import org.fisico.virtualcroupier.game.GamePhase;

public class BaseGamePhase implements GamePhase {
    int ordinal;

    public static final int SIGNUPS = 1;
    public static final int STARTED = 2;
    public static final int ENDED = 3;

    private BaseGamePhase() {
        // For morphia
    }

    public BaseGamePhase(int state) {
        ordinal = state;
    }

    public void setState(int state) {
        ordinal = state;
    }

    @Override
    public int getState() {
        return ordinal;
    }

    @Override
    public String getStateName() {
        switch (ordinal) {
            case SIGNUPS:
                return "Game is in signups";
            case STARTED:
                return "Game has started";
            case ENDED:
                return "Game has ended";
        }
        return null;
    }
}
