package org.fisico.virtualcroupier.game;

public class BGGInfo {

    private int lastPost;
    private int threadId;

    public BGGInfo() {
        // Don't needed, just to don't let the constructor empty
        super();
    }

    public BGGInfo(int threadId) {
        this.threadId = threadId;
    }

    public int getLastPost() {
        return lastPost;
    }

    public void setLastPost(int lastPost) {
        this.lastPost = lastPost;
    }

    public int getThreadId() {
        return threadId;
    }

    public void setThreadId(int threadId) {
        this.threadId = threadId;
    }
}
