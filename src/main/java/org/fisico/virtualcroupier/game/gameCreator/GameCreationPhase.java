package org.fisico.virtualcroupier.game.gameCreator;

import org.fisico.virtualcroupier.game.GamePhase;

public class GameCreationPhase implements GamePhase {
    int ordinal;

    public static final int CREATION = 1;

    public GameCreationPhase() {
        ordinal = CREATION;
    }

    public void setState(int state) {
        ordinal = state;
    }

    @Override
    public int getState() {
        return ordinal;
    }

    @Override
    public String getStateName() {
        switch (ordinal) {
            case CREATION:
                return "Game creation";
        }
        return null;
    }
}
