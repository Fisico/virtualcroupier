package org.fisico.virtualcroupier.game;

import org.fisico.virtualcroupier.modules.Module;

import java.util.Date;
import java.util.List;
import java.util.Properties;

public class GameInfo {
    private String gameId;
    private BGGInfo bggInfo;
    private Class<? extends Module> moduleType;
    private Properties options;
    private Date lastChecked;
    private String stateId;
    private List<String> players;
    private Integer maxNumberOfPlayers = null;
    private GamePhase gamePhase;

    public GameInfo(String gameId, BGGInfo bggInfo, Class<? extends Module> moduleType, Properties options, String stateId, List<String> players, Integer maxNumberOfPlayers, GamePhase gamePhase) {
        this.gameId = gameId;
        this.bggInfo = bggInfo;
        this.moduleType = moduleType;
        this.options = options;
        this.stateId = stateId;
        this.players = players;
        this.maxNumberOfPlayers = maxNumberOfPlayers;
        this.gamePhase = gamePhase;
    }

    public BGGInfo getBggInfo() {
        return bggInfo;
    }

    public void setBggInfo(BGGInfo bggInfo) {
        this.bggInfo = bggInfo;
    }

    public Class<? extends Module> getModuleType() {
        return moduleType;
    }

    public void setModuleType(Class<? extends Module> moduleType) {
        this.moduleType = moduleType;
    }

    public Properties getOptions() {
        return options;
    }

    public void setOptions(Properties options) {
        this.options = options;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Date getLastChecked() {
        return lastChecked;
    }

    public void setLastChecked(Date lastChecked) {
        this.lastChecked = lastChecked;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void addPlayer(String player) {
        if(maxNumberOfPlayers == null || players.size() < maxNumberOfPlayers) {
            players.add(player);
        }
    }

    public Integer getMaxNumberOfPlayers() {
        return maxNumberOfPlayers;
    }

    public void setMaxNumberOfPlayers(Integer maxNumberOfPlayers) {
        this.maxNumberOfPlayers = maxNumberOfPlayers;
    }

    public GamePhase getGamePhase() {
        return gamePhase;
    }

    public void setGamePhase(GamePhase gamePhase) {
        this.gamePhase = gamePhase;
    }
}
