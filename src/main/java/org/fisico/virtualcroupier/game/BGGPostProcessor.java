package org.fisico.virtualcroupier.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fisico.geekbot.model.BGGPost;
import org.fisico.geekbot.transform.BGGFormatter;
import org.fisico.virtualcroupier.command.CommandResult;
import org.fisico.virtualcroupier.command.CommandScrapper;
import org.fisico.virtualcroupier.command.VCCommand;
import org.fisico.virtualcroupier.modules.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BGGPostProcessor{

    private CommandScrapper scrapper;
    private BGGFormatter bggFormatter;
    private ModuleLocator moduleLocator;
    private BGGCommandProcessor commandProcessor;
    private static final Logger logger = LogManager.getLogger();

    @Autowired
    public BGGPostProcessor(CommandScrapper scrapper, BGGFormatter bggFormatter, ModuleLocator moduleLocator, BGGCommandProcessor commandProcessor) {
        this.scrapper = scrapper;
        this.bggFormatter = bggFormatter;
        this.moduleLocator = moduleLocator;
        this.commandProcessor = commandProcessor;
    }

    public CommandResult process(BGGPost post, GameInfo info) {
        CommandResult result = new CommandResult();
        String postText = post.getBody();
        String user = post.getUser();
        try {
            Module module = moduleLocator.getModule(info.getModuleType());
            List<String[]> commands = scrapper.extractCommandText(scrapper.cleanQuotes(postText));
            for (String[] commandParts : commands) {
                VCCommand command = module.getCommand(commandParts[0]);
                result = commandProcessor.processOneCommand(info, result, user, module, command, commandParts);
                if (!result.isCompleted()) {
                    break;
                }
            }
        } catch(Exception e) {
            logger.error(e);
            result.addPublicInfo("This is a bit embarrassing, something is wrong in my configuration or I am not up to date.");
            result.setCompleted(false);
        }

        return result;
    }
}
