package org.fisico.virtualcroupier.game;

import org.fisico.virtualcroupier.modules.Module;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ModuleLocator implements ApplicationContextAware {
    private ApplicationContext applicationContext;

    public Module getModule(Class<? extends Module> moduleType) {
        return applicationContext.getBean(moduleType);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
