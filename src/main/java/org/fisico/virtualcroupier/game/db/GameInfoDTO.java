package org.fisico.virtualcroupier.game.db;

import org.bson.types.ObjectId;
import org.fisico.virtualcroupier.game.BGGInfo;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.Date;
import java.util.List;
import java.util.Properties;

@Entity("gameinfo")
public class GameInfoDTO {

    @Id
    private ObjectId gameId;
    private BGGInfo bggInfo;
    private Class<? extends Module> moduleType;
    private Properties options;
    private Date lastChecked;
    private String stateId;
    private List<String> players;
    private Integer maxNumberOfPlayers = null;
    private GamePhase gamePhase;

    public GameInfoDTO() {

    }

    public GameInfoDTO(GameInfo info) {
        if(info.getGameId() == null) {
            gameId = new ObjectId();
        } else {
            gameId = new ObjectId(info.getGameId());
        }
        bggInfo = info.getBggInfo();
        moduleType = info.getModuleType();
        options = info.getOptions();
        lastChecked = info.getLastChecked();
        stateId = info.getStateId();
        players = info.getPlayers();
        maxNumberOfPlayers = info.getMaxNumberOfPlayers();
        gamePhase = info.getGamePhase();
    }

    public ObjectId getGameId() {
        return gameId;
    }

    public void setGameId(ObjectId gameId) {
        this.gameId = gameId;
    }

    public BGGInfo getBggInfo() {
        return bggInfo;
    }

    public void setBggInfo(BGGInfo bggInfo) {
        this.bggInfo = bggInfo;
    }

    public Class<? extends Module> getModuleType() {
        return moduleType;
    }

    public void setModuleType(Class<? extends Module> moduleType) {
        this.moduleType = moduleType;
    }

    public Properties getOptions() {
        return options;
    }

    public void setOptions(Properties options) {
        this.options = options;
    }

    public Date getLastChecked() {
        return lastChecked;
    }

    public void setLastChecked(Date lastChecked) {
        this.lastChecked = lastChecked;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }

    public Integer getMaxNumberOfPlayers() {
        return maxNumberOfPlayers;
    }

    public void setMaxNumberOfPlayers(Integer maxNumberOfPlayers) {
        this.maxNumberOfPlayers = maxNumberOfPlayers;
    }

    public GamePhase getGamePhase() {
        return gamePhase;
    }

    public void setGamePhase(GamePhase gamePhase) {
        this.gamePhase = gamePhase;
    }

    public GameInfo toGameInfo() {
        return new GameInfo(gameId.toHexString(), bggInfo, moduleType, options, stateId, players, maxNumberOfPlayers, gamePhase);
    }
}
