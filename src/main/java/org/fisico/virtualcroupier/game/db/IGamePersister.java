package org.fisico.virtualcroupier.game.db;

import java.util.List;

import org.bson.types.ObjectId;
import org.fisico.virtualcroupier.game.GameInfo;

public interface IGamePersister {
	void saveGameInfo(GameInfo info);
	List<GameInfo> retrieveDueTimeGames(int waitTime);
	void removeGameInfo(String id);
}
