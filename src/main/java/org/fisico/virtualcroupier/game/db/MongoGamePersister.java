package org.fisico.virtualcroupier.game.db;

import java.util.*;

import org.bson.types.ObjectId;
import org.fisico.virtualcroupier.game.GameInfo;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import com.mongodb.MongoClient;
import org.springframework.stereotype.Repository;

@Repository
public class MongoGamePersister implements IGamePersister {

	private Datastore datastore;
	
	public MongoGamePersister() {
		Morphia morphia = new Morphia();
		morphia.getMapper().getOptions().setStoreEmpties(true);
		morphia.getMapper().getOptions().setStoreNulls(true);
		morphia.mapPackage("org.fisico.VirtualCroupier.game");
		datastore = morphia.createDatastore(new MongoClient(), "virtualcroupier");
	}
	
	@Override
	public void saveGameInfo(GameInfo info) {
		GameInfoDTO dto = new GameInfoDTO(info);
		datastore.save(dto);
		info.setGameId(dto.getGameId().toHexString());
	}

	@Override
	public List<GameInfo> retrieveDueTimeGames(int waitTime) {
		Date dueDate;
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MINUTE, -waitTime);
		dueDate = cal.getTime();
		Query<GameInfoDTO> query = datastore.createQuery(GameInfoDTO.class).field("lastChecked").lessThan(dueDate);
		List<GameInfoDTO> dtoList = query.asList();
        List<GameInfo> list = new ArrayList<GameInfo>();
        for(GameInfoDTO dto: dtoList) {
            list.add(dto.toGameInfo());
        }
		
		return list;
	}

	@Override
	public void removeGameInfo(String id) {
        ObjectId objectId = new ObjectId(id);
		datastore.delete(datastore.createQuery(GameInfo.class).field("stateId").equal(objectId));
	}
}
