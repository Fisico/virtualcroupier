package org.fisico.virtualcroupier.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fisico.geekbot.transform.BGGFormatter;
import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.command.ModuleCommand;
import org.fisico.virtualcroupier.command.SignupCommand;
import org.fisico.virtualcroupier.components.db.IComponentPersister;
import org.fisico.virtualcroupier.modules.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BGGCommandProcessor {

    private BGGFormatter bggFormatter;
    private CommandFormatter commandFormatter;
    private IComponentPersister persister;
    private static final Logger logger = LogManager.getLogger();

    @Autowired
    public BGGCommandProcessor(BGGFormatter bggFormatter, CommandFormatter commandFormatter, IComponentPersister persister) {
        this.bggFormatter = bggFormatter;
        this.commandFormatter = commandFormatter;
        this.persister = persister;
    }

    public CommandResult processOneCommand(GameInfo info, CommandResult result, String user, Module module, VCCommand command, String[] commandParts) {
        try {
            if (command != null) {
                if (SignupCommand.class.isAssignableFrom(command.getClass()) || isParticipant(info, user)) {
                    String commandText = bggFormatter.bold(commandFormatter.format(commandParts));
                    result.addPublicInfo(bggFormatter.quote(user, commandText));
                    executeIndividualCommand(info, result, user, module, commandParts, command);
                    result.setCompleted(true);
                    logger.debug("User " + user + " executed command " + commandParts);
                }
            } else {
                result.addPublicInfo("I am truly sorry, but I know nothing about a command called " + commandParts[0] + ".");
                result.setCompleted(false);
                logger.error("User " + user + " tried to execute non-existant command  " + commandParts);
            }
        } catch (ImpossibleCommandException e) {
            result.addPublicInfo("Sorry, I encountered a problem executing the command. " + e.getMessage());
            result.setCompleted(false);
            logger.error(e);
        } catch(Exception e) {
            result.addPublicInfo("There is a problem with the module " + module.getName() + ". Contact the author with this information:\nFaulty command: " + command.getName() + "\nProblem: " + e.getMessage());
            result.setCompleted(false);
            logger.error(e);
        }
        return result;
    }

    private void executeIndividualCommand(GameInfo info, CommandResult result, String user, Module module, String[] commandParts, VCCommand command) throws ImpossibleCommandException {
            manageTableState(info, result, command);
            manageGameInfo(info, command);
            manageModule(module, command);
            validateInput(user, commandParts, command);
            validatePhase(user, commandParts, info, command);
            CommandResult partialResult = command.execute(user, commandParts);
            result.combine(partialResult);
            manageFinalState(result, command);
    }

    private void manageFinalState(CommandResult result, VCCommand command) {
        if (TableCommand.class.isAssignableFrom(command.getClass())) {
            TableCommand tableCommand = (TableCommand) command;
            result.setDesiredState(tableCommand.getState());
        }
    }

    private void manageTableState(GameInfo info, CommandResult result, VCCommand command) {
        if (TableCommand.class.isAssignableFrom(command.getClass())) {
            TableCommand tableCommand = (TableCommand) command;
            if (result.getDesiredState() == null) {
                tableCommand.setState(persister.loadTableState(info.getStateId()));
            } else {
                tableCommand.setState(result.getDesiredState());
            }
        }
    }

    private void manageGameInfo(GameInfo info, VCCommand command) {
        if (GameCommand.class.isAssignableFrom(command.getClass())) {
            GameCommand gameCommand = (GameCommand) command;
            gameCommand.setGameInfo(info);
        }
    }

    private void manageModule(Module module, VCCommand command) {
        if (ModuleCommand.class.isAssignableFrom(command.getClass())) {
            ModuleCommand moduleCommand = (ModuleCommand) command;
            moduleCommand.setModule(module);
        }
    }

    private void validateInput(String user, String[] commandParts, VCCommand command) throws ImpossibleCommandException {
        if (commandParts.length < command.getMinNumParameters() || commandParts.length > command.getMaxNumParameters()) {
            throw new ImpossibleCommandException("the format of the command is not correct. Type {help " + command.getName() + "} for details.", user, commandParts);
        }
    }

    private void validatePhase(String user, String[] commandParts, GameInfo info, VCCommand command) throws ImpossibleCommandException {
        boolean validPhase = false;
        for(GamePhase phase : command.getValidPhases()) {
            if( info.getGamePhase().getState() == phase.getState()) {
                validPhase = true;
            }
        }
        if(!validPhase) {
            throw new ImpossibleCommandException( info.getGamePhase().getStateName() + ", the command is not valid now.", user, commandParts);
        }
    }

    private boolean isParticipant(GameInfo info, String user) {
        return info.getPlayers().stream().filter(p -> p.equalsIgnoreCase(user)).count() > 0;
    }
}
