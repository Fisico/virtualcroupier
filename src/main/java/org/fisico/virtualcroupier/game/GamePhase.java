package org.fisico.virtualcroupier.game;

public interface GamePhase {
    int getState();
    String getStateName();
}
