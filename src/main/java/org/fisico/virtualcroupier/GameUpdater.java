package org.fisico.virtualcroupier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fisico.geekbot.controller.IGeekBot;
import org.fisico.geekbot.controller.actions.authentication.Credentials;
import org.fisico.geekbot.model.BGGPost;
import org.fisico.geekbot.model.BGGThread;
import org.fisico.virtualcroupier.command.CommandResult;
import org.fisico.virtualcroupier.command.MailInfo;
import org.fisico.virtualcroupier.components.db.IComponentPersister;
import org.fisico.virtualcroupier.game.BGGInfo;
import org.fisico.virtualcroupier.game.BGGPostProcessor;
import org.fisico.virtualcroupier.game.GameInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class GameUpdater {

    private BGGPostProcessor bggPostProcessor;
    private IGeekBot bot;
    private IComponentPersister componentPersister;
    private static final Logger logger = LogManager.getLogger();
    private Credentials credentials;

    @Autowired
    public GameUpdater(BGGPostProcessor bggPostProcessor, IGeekBot bot, IComponentPersister componentPersister, Credentials credentials) {
        this.bggPostProcessor = bggPostProcessor;
        this.bot = bot;
        this.componentPersister = componentPersister;
        this.credentials = credentials;
    }

    public void update(GameInfo info) {
        boolean ignoring = false;
        BGGInfo bggInfo = info.getBggInfo();
        int lastPost = bggInfo.getLastPost();

        BGGThread thread = null;
        thread = getLastPosts(bggInfo, lastPost);

        if( thread != null ) {
            List<BGGPost> posts = thread.getArticles();
            if (posts != null) {
                for (BGGPost post : posts) {
                    if(!ignoring && post.getId() != lastPost) {
                        CommandResult result = bggPostProcessor.process(post, info);
                        if (!result.isCompleted()) {
                            result.addPublicInfo("Due to this error, all the next posts until the current moment will be ignored. Excuse the incovenience. Please, feel free to post new commands from this moment on.");
                            ignoring = true;
                        }
                        if (publishResult(post, result)) {
                            // The results are published, the new state (if any) becomes official and should be saved
                            if( result.getDesiredState() != null) {
                                componentPersister.saveTableState(result.getDesiredState());
                            }
                            bggInfo.setLastPost(post.getId());
                        } else {
                            break;
                        }
                    } else {
                        bggInfo.setLastPost(post.getId());
                    }
                }
            }
        }
        info.setLastChecked(new Date());
    }

    private boolean publishResult(BGGPost post, CommandResult result) {
        boolean publishDone = true;
        if( result.getPublicInfo() != null) {
            try {
                bot.replyPost(credentials, post.getId(), post.getSubject(), result.getPublicInfo());
            } catch (Exception e) {
                publishDone = false;
                logger.error("It was not possible to publish in the board the results.", e);
            }
        }
        if(result.getPrivateInfo() != null && result.getPrivateInfo().size() > 0) {
            for(MailInfo mail : result.getPrivateInfo()) {
                try {
                    bot.sendGeekMail(credentials, mail.getUser(), post.getSubject(), mail.getContent());
                } catch (Exception e) {
                    publishDone = false;
                    logger.error("It was not possible to send a GeekMail to user " + mail.getUser(), e);
                }
            }
        }

        return publishDone;
    }

    private BGGThread getLastPosts(BGGInfo bggInfo, int lastPost) {
        BGGThread thread = null;
        try {
            if(lastPost != -1) {
                thread = bot.retrievePartialThread(bggInfo.getThreadId(), lastPost);
            } else {
                thread = bot.retrieveCompleteThread(bggInfo.getThreadId());
            }
            return thread;
        } catch (Exception e) {
            throw new VirtualCroupierException("Could not retrieve posts from BGG.", "threadid = " + bggInfo.getThreadId() + " and lastpost = " + lastPost, e );
        }
    }
}
