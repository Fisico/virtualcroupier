package org.fisico.virtualcroupier;

import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.db.IGamePersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;

import java.util.List;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

@Controller
public class GamesWatchDog {
    private static final Logger logger = LogManager.getLogger();
	private IGamePersister persister;
    private GameUpdater updater;
	private final int WAIT_TIME = 1;
    private final int ONE_MINUTE = 60000;
	volatile private boolean stopRequested;

	@Autowired
	public GamesWatchDog(IGamePersister persister, GameUpdater updater) {
		this.persister = persister;
        this.updater = updater;
		this.stopRequested = false;
	}

    @Async
	public void controlFlow() {
        System.out.print("Working");
		while(!stopRequested) {
            updateGames();
            System.out.print(".");
		}
	}

    public void updateGames() {
        try {
            List<GameInfo> dueGames = persister.retrieveDueTimeGames(WAIT_TIME);
            if (dueGames != null && !dueGames.isEmpty()) {
                for (GameInfo info : dueGames) {
                    workInGame(info);
                }
            }
        } catch(Exception e) {
            logger.error(e);
        } finally {
            try {
                Thread.sleep(ONE_MINUTE);
            } catch (InterruptedException e) {
                stopRequested = true;
            }
        }
    }

    private void workInGame(GameInfo info) {
        try {
            updater.update(info);
            persister.saveGameInfo(info);
        } catch (Exception e) {
            throw new VirtualCroupierException("Error in game", "gameId[" +info.getGameId() +"]", e );
        }
    }

    public void requestStop() {
		stopRequested = true;
	}
}
