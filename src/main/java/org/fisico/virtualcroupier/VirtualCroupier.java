package org.fisico.virtualcroupier;

import org.fisico.geekbot.controller.actions.authentication.BGGAuthenticationException;
import org.fisico.geekbot.model.BGGSideError;
import org.fisico.virtualcroupier.modules.Module;
import org.fisico.virtualcroupier.modules.ModuleRegister;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.Properties;

public class VirtualCroupier {
    private static final String CONFIG_PATH = "classpath*:application-config.xml";

    public static void main(String[] args) throws BGGAuthenticationException, IOException, BGGSideError {
        final ApplicationContext context = new ClassPathXmlApplicationContext(CONFIG_PATH);
        //final GameCreator creator = context.getBean(GameCreator.class);
        //final ModuleRegister register = context.getBean(ModuleRegister.class);
        //final Module module = register.find("GameCreator");
        //Properties options = new Properties();
        //options.put("scenario", "cataclysm");
        //creator.createGame(module, options);

        final GamesWatchDog gamesWatchDog = context.getBean(GamesWatchDog.class);
        gamesWatchDog.controlFlow();
    }
}
