package org.fisico.virtualcroupier.modules;

import org.fisico.virtualcroupier.command.VCCommand;
import org.fisico.virtualcroupier.components.TableState;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.gameCreator.GameCreationPhase;
import org.fisico.virtualcroupier.modules.Module;
import org.fisico.virtualcroupier.modules.ModuleRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

@Component
public class GameCreatorModule implements Module {

    public static final int TEST_FORUM_ID = 7;
    private Map<String,VCCommand> commandMap;

    @Autowired
    public GameCreatorModule(ModuleRegister register) {
        register.register("GameCreator", this);
    }

    @Resource(name = "gameCreatorCommands")
    public void setCommandMap(Map<String,VCCommand> commandMap) {
        this.commandMap = commandMap;
    }

    @Override
    public String getName() {
        return "GameCreator";
    }

    @Override
    public VCCommand getCommand(String name) {
        if(name == null) {
            return null;
        }
        String caseName = name.toLowerCase();
        if(commandMap.containsKey(caseName)){
            return commandMap.get(caseName);
        }
        return null;
    }

    @Override
    public Map<String, VCCommand> getAllCommands() {
        Map<String,VCCommand> all = new HashMap<>();
        all.putAll(commandMap);

        return all;
    }

    @Override
    public int getForumId() {
        return TEST_FORUM_ID;
    }

    @Override
    public String getTitle(Properties options) {
        return "VirtualCroupier game creation";
    }

    @Override
    public String getIntro(Properties options) {
        return "Use this thread to create games ";
    }

    @Override
    public TableState getInitialTableState(Properties options) {
        return new TableState();
    }

    @Override
    public GamePhase getInitialGamePhase() {
        return new GameCreationPhase();
    }

    @Override
    public List<String> getOptions() {
        return new ArrayList<>();
    }

    @Override
    public Map<String, String> getOptionsHelp() {
        return new HashMap<>();
    }
}
