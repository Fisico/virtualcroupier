package org.fisico.virtualcroupier.modules;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class ModuleRegister {
    private Map<String,Module> modules;

    public ModuleRegister() {
        modules = new HashMap<String,Module>();
    }

    public void register(String name, Module module) {
        modules.put(name, module);
    }

    public Module find(String name) {
        return modules.get(name);
    }

    public Set<String> findAll() {
        return modules.keySet();
    }
}
