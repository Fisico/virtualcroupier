package org.fisico.virtualcroupier.modules;

import org.fisico.virtualcroupier.command.VCCommand;
import org.fisico.virtualcroupier.components.TableState;
import org.fisico.virtualcroupier.game.GamePhase;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public interface Module {
    String getName();
    VCCommand getCommand(String name);
    Map<String,VCCommand> getAllCommands();
    int getForumId();
    String getTitle(Properties options);
    String getIntro(Properties options);
    TableState getInitialTableState(Properties options);
    GamePhase getInitialGamePhase();
    List<String> getOptions();
    Map<String,String> getOptionsHelp();
}
