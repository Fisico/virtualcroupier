package org.fisico.virtualcroupier.modules;

import org.fisico.virtualcroupier.command.VCCommand;
import org.fisico.virtualcroupier.components.*;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

@Component
public class BaseModule implements Module {

    public static final int TEST_FORUM_ID = 7;
    private Map<String,VCCommand> commandMap;

    @Autowired
    public BaseModule(ModuleRegister register) {
        register.register("base", this);
    }

    @Resource(name = "baseCommands")
    public void setCommandMap(Map<String,VCCommand> commandMap) {
        this.commandMap = commandMap;
    }

    @Override
    public String getName() {
        return "Base";
    }

    @Override
    public VCCommand getCommand(String name) {
        if(name == null) {
            return null;
        }
        String caseName = name.toLowerCase();
        if(commandMap.containsKey(caseName)){
            return commandMap.get(caseName);
        }
        return null;
    }

    @Override
    public Map<String,VCCommand> getAllCommands() {
        Map<String,VCCommand> all = new HashMap<>();
        all.putAll(commandMap);

        return all;
    }

    @Override
    public int getForumId() {
        return TEST_FORUM_ID;
    }

    @Override
    public String getTitle(Properties options) {
        return "Test game";
    }

    @Override
    public String getIntro(Properties options) {
        StringBuilder sb = new StringBuilder();
        sb.append("Example of game. There is a deck of ten cards numbered from 1 to ten. The player will have a hand of three cards. Every turn the player will discard one card and draw another. At the end of the round if the hand consist of three consecutive numbers the player win if the deck gets depleted the player lose.");

        return sb.toString();
    }

    @Override
    public TableState getInitialTableState(Properties options) {
        TableState state = new TableState();
        Area mainArea = new Area("table", "virtualcroupier");
        state.addArea(mainArea);
        mainArea.addDeck(new Deck("discard"));
        mainArea.addDeck(new Deck("hand"));
        Deck deck = new Deck("deck");
        mainArea.addDeck(deck);
        for(int i = 1; i <= 10 ; i++ ) {
            deck.put(new DeckOrderPosition(i), new Card("Card " + i));
        }

        return state;
    }

    @Override
    public GamePhase getInitialGamePhase() {
        return new BaseGamePhase(BaseGamePhase.SIGNUPS);
    }

    @Override
    public List<String> getOptions() {
        return new ArrayList<>();
    }

    @Override
    public Map<String, String> getOptionsHelp() {
        return new HashMap<>();
    }
}
