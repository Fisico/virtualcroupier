package org.fisico.virtualcroupier;

import org.fisico.geekbot.controller.IGeekBot;
import org.fisico.geekbot.controller.actions.authentication.BGGAuthenticationException;
import org.fisico.geekbot.controller.actions.authentication.Credentials;
import org.fisico.geekbot.model.BGGSideError;
import org.fisico.geekbot.model.GeekBotException;
import org.fisico.virtualcroupier.components.TableState;
import org.fisico.virtualcroupier.components.db.IComponentPersister;
import org.fisico.virtualcroupier.game.BGGInfo;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.db.IGamePersister;
import org.fisico.virtualcroupier.modules.Module;
import org.fisico.virtualcroupier.utils.IntegerValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

@Component
public class GameCreator {

    private IGeekBot bot;
    private IGamePersister gamePersister;
    private IComponentPersister componentPersister;
    private Credentials credentials;

    @Autowired
    public GameCreator(IGeekBot bot, IGamePersister gamePersister, IComponentPersister componentPersister, Credentials credentials) {
        this.bot = bot;
        this.gamePersister = gamePersister;
        this.componentPersister = componentPersister;
        this.credentials = credentials;
    }

    public int createGame(Module module, Properties options) throws BGGAuthenticationException, BGGSideError, GeekBotException {
//        try {

            TableState state = module.getInitialTableState(options);
            String maxPlayersProperty = options.getProperty("maxPlayers");
            Integer maxPlayers = null;
            if(maxPlayersProperty != null && IntegerValidator.isInteger(maxPlayersProperty)) {
                maxPlayers = Integer.parseInt(maxPlayersProperty);
            }
            int threadId = bot.createThread(credentials, module.getForumId(), module.getTitle(options), module.getIntro(options));
            if(threadId == -1) {
                return threadId;
            }
            BGGInfo bggInfo = new BGGInfo(threadId);
            GameInfo info = new GameInfo(null, bggInfo, module.getClass(), options, state.getId(), new ArrayList<String>(), maxPlayers, module.getInitialGamePhase() );
            info.setLastChecked(new Date());
            componentPersister.saveTableState(state);
            gamePersister.saveGameInfo(info);

            return threadId;
//        } catch (BGGAuthenticationException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (BGGSideError bggSideError) {
//            bggSideError.printStackTrace();
//        }
    }
}
