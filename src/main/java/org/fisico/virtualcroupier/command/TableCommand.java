package org.fisico.virtualcroupier.command;

import org.fisico.virtualcroupier.components.TableState;

public interface TableCommand extends VCCommand {
    TableState getState();
    void setState(TableState state);
}
