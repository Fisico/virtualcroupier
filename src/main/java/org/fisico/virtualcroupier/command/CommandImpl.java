package org.fisico.virtualcroupier.command;

import org.fisico.virtualcroupier.components.CanonicalPosition;
import org.fisico.virtualcroupier.components.Deck;
import org.fisico.virtualcroupier.components.TableState;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.modules.Module;
import org.fisico.virtualcroupier.utils.IntegerValidator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Properties;

public abstract class CommandImpl implements TableCommand, GameCommand, ModuleCommand {
    protected TableState state;
    protected GameInfo info;
    protected Module module;
    protected Properties properties;
    protected IntegerValidator validator;

    @Autowired
    public void setIntegerValidator(IntegerValidator validator) {
        this.validator = validator;
    }

    public TableState getState() {
        return state;
    }

    public void setState(TableState state) {
        this.state = state;
    }

    @Override
    public GameInfo getGameInfo() {
        return info;
    }

    @Override
    public void setGameInfo(GameInfo info) {
        this.info = info;
    }

    @Override
    public Module getModule() {
        return module;
    }

    @Override
    public void setModule(Module module) {
        this.module = module;
    }

    public int locateCard(Deck deck, String cardName) {
        int cardPos = Deck.NOT_FOUND;
        int orderPos = Deck.NOT_FOUND;
        if(validator.isInteger(cardName)) {
            try {
                cardPos = Integer.parseInt(cardName);
            } catch (NumberFormatException nfe) {
                cardPos = Deck.NOT_FOUND;
            }
        } else {
            cardPos = Deck.NOT_FOUND;
        }
        if(cardPos == Deck.NOT_FOUND) {
            orderPos = deck.locate(cardName).value();
        } else {
            orderPos = deck.locate(new CanonicalPosition(cardPos)).value();
        }

        return orderPos;
    }
}
