package org.fisico.virtualcroupier.command;

import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.modules.Module;

public interface VCCommand {
    int getMinNumParameters();
    int getMaxNumParameters();
    GamePhase[] getValidPhases();
    String getName();
    String getShortDescription();
    String getHelp();
    CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException;
}
