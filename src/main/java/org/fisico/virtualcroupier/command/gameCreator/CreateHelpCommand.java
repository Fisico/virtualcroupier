package org.fisico.virtualcroupier.command.gameCreator;

import org.fisico.virtualcroupier.command.CommandResult;
import org.fisico.virtualcroupier.command.ImpossibleCommandException;
import org.fisico.virtualcroupier.command.SignupCommand;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.gameCreator.GameCreationPhase;
import org.fisico.virtualcroupier.modules.Module;
import org.fisico.virtualcroupier.modules.ModuleRegister;

import java.util.List;
import java.util.Map;

public class CreateHelpCommand implements SignupCommand {

    private ModuleRegister moduleRegister;

    public CreateHelpCommand(ModuleRegister moduleRegister) {
        this.moduleRegister = moduleRegister;
    }

    @Override
    public int getMinNumParameters() {
        return 1;
    }

    @Override
    public int getMaxNumParameters() {
        return 1;
    }

    @Override
    public GamePhase[] getValidPhases() {
        return new GamePhase[]{ new GameCreationPhase()};
    }

    @Override
    public String getName() {
        return "CreateHelp";
    }

    @Override
    public String getShortDescription() {
        return "List the options to create a specific kind of game.";
    }

    @Override
    public String getHelp() {
        StringBuilder sb = new StringBuilder();
        sb.append("{createhelp module}");
        sb.append("\n");
        sb.append("module: Name of the kind of game");
        sb.append("\n");
        sb.append("\n");
        sb.append("Posible kinds of game:");
        for(String s : moduleRegister.findAll()) {
            sb.append(s);
            sb.append("\n");
        }

        return sb.toString();    }

    @Override
    public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
        CommandResult result = new CommandResult();
        Module module = moduleRegister.find(commandParts[1]);
        if(module == null) {
            throw  new ImpossibleCommandException("I do not know a kind of game named " + commandParts[1] + ".", user, commandParts);
        }
        List<String> keys = module.getOptions();
        Map<String,String> help = module.getOptionsHelp();
        if(keys.size() == 0) {
            result.addPublicInfo("There are no additional options beyond the kind of the game.");
        } else {
            result.addPublicInfo("The options available are, in this order:");
            for(String key : keys) {
                result.addPublicInfo(key + ": " + help.get(key));
            }
        }

        return result;
    }
}
