package org.fisico.virtualcroupier.command.gameCreator;

import org.fisico.geekbot.transform.BGGFormatter;
import org.fisico.virtualcroupier.GameCreator;
import org.fisico.virtualcroupier.command.CommandResult;
import org.fisico.virtualcroupier.command.ImpossibleCommandException;
import org.fisico.virtualcroupier.command.VCCommand;
import org.fisico.virtualcroupier.command.SignupCommand;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.gameCreator.GameCreationPhase;
import org.fisico.virtualcroupier.modules.Module;
import org.fisico.virtualcroupier.modules.ModuleRegister;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Properties;

public class CreateCommand implements SignupCommand {

    private ModuleRegister moduleRegister;
    private GameCreator gameCreator;
    private BGGFormatter bggFormatter;

    public CreateCommand(ModuleRegister moduleRegister, GameCreator gameCreator, BGGFormatter bggFormatter) {
        this.moduleRegister = moduleRegister;
        this.gameCreator = gameCreator;
        this.bggFormatter = bggFormatter;
    }

    @Override
    public int getMinNumParameters() {
        return 1;
    }

    @Override
    public int getMaxNumParameters() {
        return Integer.MAX_VALUE;
    }

    @Override
    public GamePhase[] getValidPhases() {
        return new GamePhase[]{ new GameCreationPhase()};
    }

    @Override
    public String getName() {
        return "Create";
    }

    @Override
    public String getShortDescription() {
        return "Create a game for a module";
    }

    @Override
    public String getHelp() {
        StringBuilder sb = new StringBuilder();
        sb.append("{create module options}");
        sb.append("\n");
        sb.append("module: Name of the kind of game");
        sb.append("\n");
        sb.append("options: A list of game options specific for the kind of game separated by space. To get a list of the possible values type {createhelp module}");
        sb.append("\n");
        sb.append("\n");
        sb.append("Posible games to create:");
        for(String s : moduleRegister.findAll()) {
            sb.append(s);
            sb.append("\n");
        }

        return sb.toString();
    }

    @Override
    public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
        try {
            CommandResult result = new CommandResult();
            Module module = moduleRegister.find(commandParts[1]);
            if(module == null) {
                throw  new ImpossibleCommandException("I do not know how to create a game of " + commandParts[1] + ".", user, commandParts);
            }
            List<String> keys = module.getOptions();
            if(commandParts.length < keys.size() -2) {
                throw new ImpossibleCommandException("there are too few options in the command, this kind of game needs " + keys.size() + ".", user, commandParts);
            }
            Properties options = new Properties();
            for(int index = 0 ; index < keys.size(); index++) {
                options.put(keys.get(index), commandParts[index+2]);
            }
            int threadId = gameCreator.createGame(module, options);
            if( threadId != -1) {
                result.setPublicInfo("The game was created successfully here " + bggFormatter.thread(threadId));
            } else {
                throw  new ImpossibleCommandException("I was incapable of creating a thread for the game. Please, try again later and, if the problem persist, send a GeekMail to Fisico.",user, commandParts);
            }
            return result;
        } catch(Exception e) {
            throw  new ImpossibleCommandException("there was an error creating the game. Maybe some of the options are invalid?", user, commandParts);
        }
    }
}
