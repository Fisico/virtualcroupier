package org.fisico.virtualcroupier.command;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommandScrapper {

    private static final String CLOSE_COMMAND = "}";
	private static final String OPEN_COMMAND = "{";
	private static final String CLOSE_QUOTE = "</div></div></font>";
	private static final String OPEN_QUOTE = "<font color=#2121A4><div class='quote'><div class='quotebody'><i>";
    private static final String OPEN_QUOTE_ALT = "<font color=#0B5C0D><div class='quote'><div class='quotebody'><i>";
    private static final String CLOSE_QUOTE_USER = "</div>";
	private static final String OPEN_QUOTE_USER = "<div class='quotetitle'>";
	private static final String OPEN_BOLD = "<b>";
	private static final String CLOSE_BOLD = "</b>";

	public List<String[]> extractCommandText(String text) {//extractCommandText

        List<String[]> list = new ArrayList<String[]>();

        for(String commandText: sliceText(text))
        {
            if (commandText.startsWith(OPEN_COMMAND) && commandText.endsWith(CLOSE_COMMAND) && commandText.length() > 2) {
                commandText = commandText.substring(1, commandText.length() - 1);

                String[] command = commandText.split(" ");
                List<String> cleanedCommand = new ArrayList<String>();
                for (String part : command) {
                    if (!"".equalsIgnoreCase(part.trim())) {
                        cleanedCommand.add(part);
                    }
                }
                if (cleanedCommand.size() > 0) {
                    list.add(cleanedCommand.toArray(new String[0]));
                }
            }
        }

        return list;
    }

    private List<String> sliceText(String text)
    {
        List<String> list = new ArrayList<String>();

        int close = text.indexOf(CLOSE_BOLD);
        int open = text.indexOf(OPEN_BOLD);

        if (open != -1 && close != -1) {
            String commandText = text.substring(open + OPEN_BOLD.length() , close);
            commandText = commandText.trim();
            String rest = text.substring(close + CLOSE_BOLD.length());
            list.add(commandText);
            list.addAll(sliceText(rest));
        }

        return list;
    }

    public String cleanQuotes(String text) {
    	String cleaned = cleanAuthors(text);
    	cleaned = cleanSimpleQuotes(cleaned);
    	
    	return cleaned;
    }
    
    private String cleanSimpleQuotes(String text) {
        String cleaned = removeSingleQuote(text);
        String last = text;
        while (!last.equalsIgnoreCase(cleaned)) {
            last = cleaned;
            cleaned = removeSingleQuote(cleaned);
        }

        return cleaned;
    }
    
    private String cleanAuthors(String text) {
    	String anonimizedText = removeQuoteAuthor(text);
    	String last = text;
        while (!last.equalsIgnoreCase(anonimizedText)) {
            last = anonimizedText;
            anonimizedText = removeQuoteAuthor(anonimizedText);
        }

        return anonimizedText;
    }

    private String removeQuoteAuthor(String text) {
    	String res = text;
        int lastQuote = text.lastIndexOf(OPEN_QUOTE_USER);
        if (lastQuote != -1) {
        	int close = text.indexOf(CLOSE_QUOTE_USER, lastQuote);
            if (close != -1) {
                res = text.substring(0, lastQuote) + text.substring(close + CLOSE_QUOTE_USER.length());
            }
        }

        return res;
    }
    
    private String removeSingleQuote(String text) {
        String res = text;
        int endOfQuote = findFirstEndOfQuote(text);
        int startOfQuote = findLastStartOfQuoteBeforeIndex(text, endOfQuote);
        if( startOfQuote != -1 && endOfQuote != -1 ) {
            String textBeforeQuote = text.substring(0, startOfQuote);
            String textAfterQuote = text.substring(endOfQuote + CLOSE_QUOTE.length());
            res = textBeforeQuote + textAfterQuote;
        }

        return res;
    }

    private int findFirstEndOfQuote(String text) {
        int close = text.indexOf(CLOSE_QUOTE);

        return close;
    }

    private int findLastStartOfQuoteBeforeIndex(String text, int endOfQuote) {
        int start = -1;
        if(endOfQuote != -1) {
            String fragment = text.substring(0,endOfQuote);
            int startCandidate1 = fragment.lastIndexOf(OPEN_QUOTE);
            int startCandidate2 = fragment.lastIndexOf(OPEN_QUOTE_ALT);
            start = Math.max(startCandidate1,startCandidate2);
        }

        return start;
    }
}
