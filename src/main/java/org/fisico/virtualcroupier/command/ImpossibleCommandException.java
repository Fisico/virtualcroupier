package org.fisico.virtualcroupier.command;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ImpossibleCommandException extends Exception{

    private String user;
    private String[] commandParts;

	public ImpossibleCommandException(String message, String user, String[] commandParts, Throwable cause){
        super(message, cause);
        this.user = user;
        this.commandParts = commandParts;
    }

    public ImpossibleCommandException(String message, String user, String[] commandParts){
        super(message);
        this.user = user;
        this.commandParts = commandParts;
    }

    @Override
    public String toString() {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        StringBuilder sb = new StringBuilder();
        for( String part : commandParts) {
            sb.append(part);
            sb.append(" ");
        }
        String parts = sb.toString();
        String context = "";
        if(user != null) {
            context += "user=" + user + " ";
        }
        if(parts != null && !parts.trim().isEmpty()) {
            context += "parts=" + parts + " ";
        }

        //StringWriter trace = new StringWriter();
        //printStackTrace(new PrintWriter(trace));

        return s + " (" + context + "): " + message;
        //return s + " (" + context + "): " + message + trace.toString();
    }
}
