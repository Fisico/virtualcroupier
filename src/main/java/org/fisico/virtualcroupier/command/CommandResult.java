package org.fisico.virtualcroupier.command;

import org.fisico.virtualcroupier.components.TableState;

import java.util.ArrayList;
import java.util.List;

public class CommandResult {

    private static final String MSG_SEPARATOR = "\n";

    private boolean completed;
    private List<MailInfo> privateInfo;
	private String publicInfo;
    private TableState desiredState;

    public CommandResult() {
        completed = false;
        privateInfo = new ArrayList<MailInfo>();
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public List<MailInfo> getPrivateInfo() {
		return privateInfo;
	}

	public void setPrivateInfo(List<MailInfo> privateInfo) {
		this.privateInfo = privateInfo;
	}

    public TableState getDesiredState() {
        return desiredState;
    }

    public void setDesiredState(TableState desiredState) {
        this.desiredState = desiredState;
    }

    public void addPrivateInfo(MailInfo privateInfo) {
        MailInfo currentInfo = null;
        for(MailInfo info: this.privateInfo) {
            if(info.getUser().equalsIgnoreCase(privateInfo.getUser())) {
                currentInfo = info;
                break;
            }

        }
        if(currentInfo == null) {
            this.privateInfo.add(privateInfo);
        } else {
            currentInfo.setContent( currentInfo.getContent() + MSG_SEPARATOR + privateInfo.getContent() );
        }
    }

    public void addPrivateInfo(String user, String content) {
        this.privateInfo.add(new MailInfo(user, content));
    }

	public String getPublicInfo() {
		return publicInfo;
	}

	public void setPublicInfo(String publicInfo) {
		this.publicInfo = publicInfo;
	}

    public void addPublicInfo(String publicInfo) {
        if(this.publicInfo == null) {
            this.publicInfo = publicInfo;
        } else if(publicInfo != null){
            this.publicInfo = this.publicInfo + MSG_SEPARATOR + publicInfo;
        }
    }

    public void combine(CommandResult partial) {
        String partialPublic = partial.getPublicInfo();
        addPublicInfo(partialPublic);
        for(MailInfo infoToAdd : partial.getPrivateInfo()) {
            addPrivateInfo(infoToAdd);
        }
    }
}
