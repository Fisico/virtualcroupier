package org.fisico.virtualcroupier.command.base;

import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.components.Deck;
import org.fisico.virtualcroupier.components.DeckOrderPosition;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;

public class DrawCommand extends CommandImpl {

    @Override
    public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
    	CommandResult result = new CommandResult();
        Deck deckDrawn = state.locateDeck(commandParts[1], user);
        Deck deckTo  = state.locateDeck(commandParts[2], user);
        int numCards = 1;
        if(commandParts.length == 4) {
            try {
                numCards = Integer.parseInt(commandParts[3]);
            } catch (NumberFormatException nfe) {
                throw new ImpossibleCommandException("The command draw has wrong arguments. type {help draw} for details.", user, commandParts);
            }
        }
    	if (numCards < 1) {
            throw new ImpossibleCommandException("Only a positive number of cards can be drawn", user, commandParts);
        }
    	if( deckDrawn == null ) {
    		throw new ImpossibleCommandException("The deck to drawn from is not found", user, commandParts);
    	}
    	if( deckTo == null ) {
    		throw new ImpossibleCommandException("The deck to put the cards drawn is not found", user, commandParts);
    	}
        if (deckDrawn.size() >= numCards) {
            for (int i = 0; i < numCards; i++) {
				deckTo.put(DeckOrderPosition.BOTTOM, deckDrawn.draw());
            }
        } else {
            throw new ImpossibleCommandException("Not enough cards in deck", user, commandParts);
        }
        result.setPublicInfo(deckTo.toString());

        return result;
    }

	@Override
	public int getMinNumParameters() {
		return 3;
	}

	@Override
	public int getMaxNumParameters() {
		return 4;
	}

	@Override
	public GamePhase[] getValidPhases() {
		return new GamePhase[] { new BaseGamePhase(BaseGamePhase.STARTED)};
	}

	@Override
	public String getName() {
		return "Draw";
	}

	@Override
	public String getShortDescription() {
		return "Draw a number of cards from a deck";
	}

	@Override
	public String getHelp() {
		StringBuilder sb = new StringBuilder();
		sb.append("{draw from to number?}");
		sb.append("\n");
		sb.append("from: the deck from witch the cards are drawn");
		sb.append("\n");
		sb.append("to: the deck where to put the cards drawn");
		sb.append("\n");
		sb.append("number (optional): the number of cards to draw. If it is not provided is 1 by default.");
		sb.append("\n");
		
		return sb.toString();
	}
}
