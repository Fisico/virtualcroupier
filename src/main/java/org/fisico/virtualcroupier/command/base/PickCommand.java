package org.fisico.virtualcroupier.command.base;

import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.components.Deck;
import org.fisico.virtualcroupier.components.DeckOrderPosition;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;

public class PickCommand extends CommandImpl {

    @Override
    public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
        CommandResult result = new CommandResult();
        Deck deckDrawn = state.locateDeck(commandParts[1], user);
        Deck deckTo  = state.locateDeck(commandParts[2], user);
        int numCards = 1;
        String cardName = commandParts[3];
        if( deckDrawn == null ) {
            throw new ImpossibleCommandException("The deck to drawn from is not found", user, commandParts);
        }
        if( deckTo == null ) {
            throw new ImpossibleCommandException("The deck to put the cards drawn is not found", user, commandParts);
        }
        int orderPos = locateCard(deckDrawn, cardName);
        if(orderPos == Deck.NOT_FOUND) {
            throw new ImpossibleCommandException("I can not found the card " + cardName, user, commandParts);
        }
        if (deckDrawn.size() >= 1) {
            deckTo.put(DeckOrderPosition.BOTTOM, deckDrawn.draw(new DeckOrderPosition(orderPos)));
        } else {
            throw new ImpossibleCommandException("Not enough cards in deck", user, commandParts);
        }
        result.setPublicInfo(deckTo.toString());

        return result;
    }

    @Override
    public int getMinNumParameters() {
        return 3;
    }

    @Override
    public int getMaxNumParameters() {
        return 4;
    }

    @Override
    public GamePhase[] getValidPhases() {
        return new GamePhase[] {new BaseGamePhase(BaseGamePhase.STARTED)};
    }

    @Override
    public String getName() {
        return "Pick";
    }

    @Override
    public String getShortDescription() {
        return "Pick a concrete card from a deck and puts it it in another one";
    }

    @Override
    public String getHelp() {
        StringBuilder sb = new StringBuilder();
        sb.append("{pick from to card}");
        sb.append("\n");
        sb.append("from: the deck from witch the cards are drawn");
        sb.append("\n");
        sb.append("to: the deck where to put the cards drawn");
        sb.append("\n");
        sb.append("card: the position shown of the card to pick or the shown name. Note: If it is the name, it could not have spaces, surround it with [ and ] or replace the spaces shown with underscores(_)");
        sb.append("\n");

        return sb.toString();
    }
}
