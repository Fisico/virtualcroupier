package org.fisico.virtualcroupier.command.base;

import java.util.List;

import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.springframework.stereotype.Service;

@Service
public class BaseSignupCommand extends CommandImpl implements SignupCommand {

	@Override
	public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
		CommandResult result = new CommandResult();
		Integer maxNumPlayers = info.getMaxNumberOfPlayers();
		List<String> players = info.getPlayers();
		int numPlayers = players.size();
		if( maxNumPlayers != null && numPlayers >= maxNumPlayers ) {
			throw new ImpossibleCommandException("game is full", user, commandParts);
		} else if(players.contains(user)) {
			throw new ImpossibleCommandException(user + " is already in this game", user, commandParts);
		} else {
			info.addPlayer(user);
			result.setPublicInfo( user + " has signed up for the game");
		}
		
		return result;
	}

	@Override
	public int getMinNumParameters() {
		return 1;
	}

	@Override
	public int getMaxNumParameters() {
		return 1;
	}

	@Override
	public GamePhase[] getValidPhases() {
		return new GamePhase[] {new BaseGamePhase(BaseGamePhase.SIGNUPS)};
	}

	@Override
	public String getName() {
		return "Signup";
	}

	@Override
	public String getShortDescription() {
		return "Sign up for this game";
	}

	@Override
	public String getHelp() {
		return "{signup}\n";
	}
}
