package org.fisico.virtualcroupier.command.base;

import org.fisico.geekbot.transform.BGGFormatter;
import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelpCommand implements ModuleCommand {

    protected Module module;
    protected BGGFormatter formatter;

    @Override
    public Module getModule() {
        return module;
    }

    @Override
    public void setModule(Module module) {
        this.module = module;
    }

    @Autowired
    public void setFormatter(BGGFormatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
        CommandResult result = new CommandResult();
        String commandAsked = null;
        if(commandParts.length == 2 ) {
            commandAsked = commandParts[1];
        }
        StringBuilder sb = new StringBuilder();
        if( commandAsked == null ) {
            formatGeneralHelp(sb);
            for (VCCommand command : module.getAllCommands().values()) {
                formatHelpOfCommand(sb, command);
            }
        } else {
            VCCommand command = module.getCommand(commandAsked);
            if(command == null) {
                throw new ImpossibleCommandException("I know nothing about the command " + commandAsked, user, commandParts);
            }
            formatHelpOfCommand(sb, command);
        }
        result.setPublicInfo(sb.toString());

        return result;
    }

    private void formatGeneralHelp(StringBuilder sb) {
        sb.append("Command should be inside brackets ({ and }) and in bold (including the brackets). The first word of the command is the command name and the rest are the parameters.");
        sb.append("\n");
        sb.append("\n");
        sb.append("[b]Commands:[/b]");
        sb.append("\n");
    }

    private void formatHelpOfCommand(StringBuilder sb, VCCommand command) {
        sb.append(formatter.bold(command.getName()));
        sb.append(": ");
        sb.append(command.getShortDescription());
        sb.append("\n");
        sb.append(command.getHelp());
        sb.append("\n");
    }

    @Override
    public int getMinNumParameters() {
        return 1;
    }

    @Override
    public int getMaxNumParameters() {
        return 2;
    }

    @Override
    public GamePhase[] getValidPhases() {
        return new GamePhase[] {new BaseGamePhase(BaseGamePhase.SIGNUPS),
                new BaseGamePhase(BaseGamePhase.STARTED),
                new BaseGamePhase(BaseGamePhase.ENDED)};
    }

    @Override
    public String getName() {
        return "Help";
    }

    @Override
    public String getShortDescription() {
        return "Show this message or get information about one specific command";
    }

    @Override
    public String getHelp() {
        return "{help} or {help <command>}\n";
    }
}
