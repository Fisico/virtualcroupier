package org.fisico.virtualcroupier.command.base;

import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.components.Card;
import org.fisico.virtualcroupier.components.Deck;
import org.fisico.virtualcroupier.components.DeckOrderPosition;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.springframework.stereotype.Service;

@Service
public class TapCommand extends CommandImpl {

    @Override
    public int getMinNumParameters() {
        return 3;
    }

    @Override
    public int getMaxNumParameters() {
        return 3;
    }

    @Override
    public GamePhase[] getValidPhases() {
        return new GamePhase[] {new BaseGamePhase(BaseGamePhase.STARTED)};
    }

    @Override
    public String getName() {
        return "Tap";
    }

    @Override
    public String getShortDescription() {
        return "Tap one card or untap it if it is already tapped.";
    }

    @Override
    public String getHelp() {
        StringBuilder sb = new StringBuilder();
        sb.append("{tap deck card}");
        sb.append("\n");
        sb.append("deck: The name of the deck in witch the card is located. It it is not provided is supossed to be a card in play");
        sb.append("\n");
        sb.append("card: The number or the name of the card you want to tap");
        sb.append("\n");

        return sb.toString();
    }

    @Override
    public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
        CommandResult result = new CommandResult();
        Deck deck;
        String deckName;
        String card;
        deckName = commandParts[1];
        card = commandParts[2];
        deck = state.locateDeck(deckName, user);
        int realPosition = locateCard(deck, card);
        if(realPosition == Deck.NOT_FOUND) {
            throw new ImpossibleCommandException("I can not found the card " + card, user, commandParts);
        }
        Card target = deck.peek(new DeckOrderPosition(realPosition));
        target.setTapped(!target.isTapped());
        result.setPublicInfo( deck.toString());

        return result;
    }
}
