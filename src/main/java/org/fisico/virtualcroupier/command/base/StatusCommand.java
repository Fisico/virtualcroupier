package org.fisico.virtualcroupier.command.base;

import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.components.Area;
import org.fisico.virtualcroupier.components.TableState;
import org.fisico.virtualcroupier.components.Visibility;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.springframework.stereotype.Service;

@Service
public class StatusCommand extends CommandImpl {


	@Override
	public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
		CommandResult result = new CommandResult();
        if(commandParts.length < 1 || commandParts.length > 2) {
            throw new ImpossibleCommandException("the format of the command is not correct.  Type {help " + getName() + "} for details.", user, commandParts);
        }
        String publicInfo = null;
        String privateInfo = null;
        if(commandParts.length == 2) {
            Area area = state.locateArea(commandParts[1], user);
            publicInfo = area.seenAs(user, Visibility.PUBLIC_AREA);
            privateInfo = area.seenAs(user, Visibility.PRIVATE_AREA);
        } else {
            publicInfo = state.seenAs(user, Visibility.PUBLIC_AREA);
            privateInfo = state.seenAs(user, Visibility.PRIVATE_AREA);
        }
        if(isMeaningful(publicInfo)) {
            result.setPublicInfo(publicInfo);
        }
        if(isMeaningful(privateInfo)) {
            result.addPrivateInfo(user, privateInfo);
        }
		return result;
	}

    private boolean isMeaningful(String content) {
        if(content == null) {
            return false;
        }
        String meaning = content.trim();
        meaning = meaning.replace("\t", "");
        meaning = meaning.replace("\n", "");

        return !"".equals(meaning);
    }

    @Override
    public int getMinNumParameters() {
        return 1;
    }

    @Override
    public int getMaxNumParameters() {
        return 2;
    }

    @Override
    public GamePhase[] getValidPhases() {
        return new GamePhase[] {new BaseGamePhase(BaseGamePhase.SIGNUPS),
                new BaseGamePhase(BaseGamePhase.STARTED),
                new BaseGamePhase(BaseGamePhase.ENDED)};
    }

    @Override
	public String getName() {
		return "Status";
	}

	@Override
	public String getShortDescription() {
        return "Show the status of the game or a particular area";
	}

	@Override
	public String getHelp() {
        StringBuilder sb = new StringBuilder();
        sb.append("{status area}");
        sb.append("\n");
        sb.append("area (optional): get information for this area");
        sb.append("\n");

        return sb.toString();
	}
}
