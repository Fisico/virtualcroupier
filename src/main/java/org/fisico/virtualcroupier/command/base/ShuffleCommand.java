package org.fisico.virtualcroupier.command.base;

import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.components.Deck;
import org.fisico.virtualcroupier.components.TableState;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.springframework.stereotype.Service;

@Service
public class ShuffleCommand extends CommandImpl {

    @Override
    public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
    	CommandResult result = new CommandResult();
		String deckName = commandParts[1];
		Deck deck = state.locateDeck(deckName, user);
		if( deck == null ) {
			throw new ImpossibleCommandException("I can not find the deck " + deckName, user, commandParts);
		}
		deck.shuffle();
        result.setPublicInfo(deck.getName() + " shuffled");

		return result;
    }

	@Override
	public int getMinNumParameters() {
		return 2;
	}

	@Override
	public int getMaxNumParameters() {
		return 2;
	}

	@Override
	public GamePhase[] getValidPhases() {
		return new GamePhase[] {new BaseGamePhase(BaseGamePhase.STARTED)};
	}

	@Override
	public String getName() {
		return "Shuffle";
	}

	@Override
	public String getShortDescription() {
		return "Shufles a deck";
	}

	@Override
	public String getHelp() {
		StringBuilder sb = new StringBuilder();
		sb.append("{shuffle deck}");
		sb.append("\n");
		sb.append("deck: the deck to shufffle");
		sb.append("\n");

		return sb.toString();
	}
}
