package org.fisico.virtualcroupier.command;

import org.springframework.stereotype.Component;

@Component
public class CommandFormatter {

    public String format(String[] commandParts) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for(int index = 0 ; index < commandParts.length ; index++) {
            sb.append(commandParts[index]);
            if( index != commandParts.length -1 ) {
                sb.append(" ");
            }
        }
        sb.append("}");

        return sb.toString();
    }
}
