package org.fisico.virtualcroupier.command;

/**
 * Created on 29/08/15.
 */
public class InvalidCommandSyntaxException extends IllegalArgumentException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6386977777328306354L;

	public InvalidCommandSyntaxException() {
        super();
    }

    public InvalidCommandSyntaxException(Exception ex) {
        super(ex);
    }

    public InvalidCommandSyntaxException(String message) {
        super(message);
    }
}
