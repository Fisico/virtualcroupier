package org.fisico.virtualcroupier.command;

import org.fisico.virtualcroupier.game.GameInfo;

public interface GameCommand extends VCCommand{
    GameInfo getGameInfo();
    void setGameInfo(GameInfo info);
}
