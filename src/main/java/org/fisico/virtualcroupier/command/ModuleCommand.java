package org.fisico.virtualcroupier.command;

import org.fisico.virtualcroupier.command.VCCommand;
import org.fisico.virtualcroupier.modules.Module;

public interface ModuleCommand extends VCCommand{
    Module getModule();
    void setModule(Module module);
}
