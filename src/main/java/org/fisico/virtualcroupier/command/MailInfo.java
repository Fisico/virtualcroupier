package org.fisico.virtualcroupier.command;

public class MailInfo {
    private String user;
    private String content;

    public MailInfo() {
        super();
    }

    public MailInfo(String user, String content) {
        this.user = user;
        this.content = content;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
