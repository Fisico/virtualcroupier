package org.fisico.virtualcroupier.command;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserResolver {
    public String resolveUser(String aproximate, List<String> players) {
        double bestScore = 0;
        String mostProbable = null;
        for(String candidate : players) {
            double points = StringUtils.getJaroWinklerDistance(aproximate, candidate);
            if(points > bestScore) {
                bestScore = points;
                mostProbable = candidate;
            }
        }

        return mostProbable;
    }
}
