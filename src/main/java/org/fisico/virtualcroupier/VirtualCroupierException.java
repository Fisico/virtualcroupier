package org.fisico.virtualcroupier;

import java.io.PrintWriter;
import java.io.StringWriter;

public class VirtualCroupierException extends RuntimeException {
    private String context;

    public VirtualCroupierException(String message, String context, Throwable cause) {
        super(message, cause);
        this.context = context;
    }

    @Override
    public String toString() {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        StringWriter trace = new StringWriter();
        printStackTrace(new PrintWriter(trace));

        return s + " (" + context + "): " + message + trace.toString();
    }

}
