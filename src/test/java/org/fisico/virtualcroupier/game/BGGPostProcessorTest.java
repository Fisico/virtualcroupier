package org.fisico.virtualcroupier.game;

import org.fisico.geekbot.transform.BGGFormatter;
import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.components.TableState;
import org.fisico.virtualcroupier.modules.Module;
import org.fisico.geekbot.model.BGGPost;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.*;

public class BGGPostProcessorTest {

    public class ExampleGamePhase implements GamePhase {
        @Override
        public int getState() {
            return 0;
        }

        @Override
        public String getStateName() {
            return null;
        }
    }

    public class ExampleCommand implements VCCommand {

        public ExampleCommand() {
        }

        @Override
        public int getMinNumParameters() {
            return 1;
        }

        @Override
        public int getMaxNumParameters() {
            return 1;
        }

        @Override
        public GamePhase[] getValidPhases() {
            return new GamePhase[] { new ExampleGamePhase()};
        }

        @Override
        public String getName() {
            return "command";
        }

        @Override
        public String getShortDescription() {
            return "description";
        }

        @Override
        public String getHelp() {
            return "help";
        }

        @Override
        public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
            return new CommandResult();
        }
    }

    public class ExampleModule implements Module {

        private ExampleCommand command;

        public ExampleModule(ExampleCommand command) {
            this.command = command;
        }

        @Override
        public String getName() {
            return "Example";
        }

        @Override
        public VCCommand getCommand(String name) {
            return command;
        }

        @Override
        public Map<String, VCCommand> getAllCommands() {
            HashMap<String, VCCommand> map = new HashMap<String, VCCommand>();
            map.put("command", command);
            return map;
        }

        @Override
        public int getForumId() {
            return 0;
        }

        @Override
        public String getTitle(Properties options) {
            return null;
        }

        @Override
        public String getIntro(Properties options) {
            return null;
        }

        @Override
        public TableState getInitialTableState(Properties options) {
            return null;
        }

        @Override
        public GamePhase getInitialGamePhase() {
            return new ExampleGamePhase();
        }

        @Override
        public List<String> getOptions() {
            return new ArrayList<>();
        }

        @Override
        public Map<String, String> getOptionsHelp() {
            return new HashMap<>();
        }
    }

    @Test
    public void noCommandPostReturnsNoResult() {
        // Setup
        BGGPost post = new BGGPost(1,"player1","subject","idle chat");
        ModuleLocator locator = Mockito.mock(ModuleLocator.class);
        Module module = Mockito.mock(Module.class);
        Mockito.when(locator.getModule(Mockito.any(Class.class))).thenReturn(module);

        ArrayList<String> playerList = new ArrayList<String>();
        playerList.add( "player1");
        List<String[]> list = new ArrayList<String[]>();
        CommandScrapper scrapper = Mockito.mock(CommandScrapper.class);
        Mockito.when( scrapper.extractCommandText((String) Mockito.isNull())).thenReturn(list);
        Mockito.when( scrapper.extractCommandText(Mockito.anyString())).thenReturn(list);

        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), playerList, null, null);
        BGGCommandProcessor processor = Mockito.mock(BGGCommandProcessor.class);

        BGGPostProcessor sut = new BGGPostProcessor(scrapper, new BGGFormatter(), locator, processor);

        // Test
        CommandResult result = sut.process(post, info);

        // Assert
        Mockito.verify(processor , Mockito.never() ).processOneCommand(Mockito.any(GameInfo.class), Mockito.any(CommandResult.class), Mockito.anyString(), Mockito.any(Module.class), Mockito.any(VCCommand.class), Mockito.any(new String[0].getClass()));
    }

    @Test
    public void aCommandPostReturnsCommandResult() {
        // Setup
        BGGPost post = new BGGPost(1,"player1","subject","[b]{command}[/b]");
        List<String[]> list = new ArrayList<String[]>();
        list.add(new String[]{"command"});
        ModuleLocator locator = Mockito.mock(ModuleLocator.class);
        Module module = Mockito.mock(Module.class);
        Mockito.when(locator.getModule(Mockito.any(Class.class))).thenReturn(module);
        CommandScrapper scrapper = Mockito.mock(CommandScrapper.class);
        Mockito.when( scrapper.extractCommandText((String) Mockito.isNull())).thenReturn(list);
        Mockito.when( scrapper.extractCommandText(Mockito.anyString())).thenReturn(list);
        ArrayList<String> playerList = new ArrayList<String>();
        playerList.add( "player1");
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), playerList, null, null);
        BGGCommandProcessor processor = Mockito.mock(BGGCommandProcessor.class);
        Mockito.when(processor.processOneCommand(Mockito.any(GameInfo.class), Mockito.any(CommandResult.class), Mockito.anyString(),Mockito.any(Module.class),Mockito.any(VCCommand.class), Mockito.any())).thenReturn(new CommandResult());

        BGGPostProcessor sut = new BGGPostProcessor(scrapper, new BGGFormatter(), locator, processor);

        // Test
        CommandResult result = sut.process(post, info);

        // Assert
        Mockito.verify(processor , Mockito.atLeastOnce() ).processOneCommand(Mockito.any(GameInfo.class), Mockito.any(CommandResult.class), Mockito.anyString(), Mockito.any(Module.class), Mockito.any(VCCommand.class), Mockito.any(new String[0].getClass()));
    }
}
