package org.fisico.virtualcroupier.game;

import org.fisico.geekbot.model.BGGPost;
import org.fisico.geekbot.transform.BGGFormatter;
import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.command.SignupCommand;
import org.fisico.virtualcroupier.components.TableState;
import org.fisico.virtualcroupier.components.db.IComponentPersister;
import org.fisico.virtualcroupier.modules.Module;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class BGGCommandProcessorTest {

    @Test
    public void commandsOfANonPlayingUserAreIgnored() {
        // Setup
        String[] list = new String[]{"command"};
        Module module = Mockito.mock(Module.class);
        IComponentPersister persister = Mockito.mock(IComponentPersister.class);

        ArrayList<String> playerList = new ArrayList<String>();
        playerList.add( "player1");
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), playerList, null, null);
        TableState state = Mockito.mock(TableState.class);
        BGGCommandProcessor sut = new BGGCommandProcessor(new BGGFormatter(), new CommandFormatter(), persister);
        CommandResult result = new CommandResult();
        VCCommand command = Mockito.mock(VCCommand.class);

        // Test
        sut.processOneCommand(info, result, "otherPlayer", module, command, list);

        // Assert
        Assert.assertNull(result.getPublicInfo());
        Assert.assertEquals(0, result.getPrivateInfo().size());
    }

    @Test
    public void signupCommandOfPlayerIsNotIgnored() throws ImpossibleCommandException {
        // Setup
        BGGPost post = new BGGPost(1,"player1", "subject", "{signup}");
        List<String[]> list = new ArrayList<String[]>();
        list.add(new String[]{"command"});
        ModuleLocator locator = Mockito.mock(ModuleLocator.class);
        CommandScrapper scrapper = Mockito.mock(CommandScrapper.class);
        Mockito.when( scrapper.extractCommandText((String) Mockito.isNull())).thenReturn(list);
        Mockito.when( scrapper.extractCommandText(Mockito.anyString())).thenReturn(list);
        Module module = Mockito.mock(Module.class);
        Mockito.when(locator.getModule(Mockito.any(Class.class))).thenReturn(module);
        IComponentPersister persister = Mockito.mock(IComponentPersister.class);

        ArrayList<String> playerList = new ArrayList<String>();
        playerList.add( "otherPlayer");
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), playerList, null, null);
        TableState state = Mockito.mock(TableState.class);
        BGGCommandProcessor sut = new BGGCommandProcessor(new BGGFormatter(), new CommandFormatter(), persister);
        CommandResult result = new CommandResult();
        VCCommand command = Mockito.mock(SignupCommand.class);
        Mockito.when(command.execute(Mockito.anyString(), Mockito.any(String[].class))).thenReturn(result);

        // Test
        sut.processOneCommand(info, result, "otherPlayer", module, command, new String[] { "command" });

        // Assert
        boolean something = (result.getPublicInfo() != null);
        something = something ||( result.getPrivateInfo().size() > 0);
        Assert.assertTrue(something);
    }

    @Test
    public void commandWithTooFewArgumentsThrowsException() {
        // Setup
        ArrayList<String> playerList = new ArrayList<String>();
        playerList.add( "player1");
        GameInfo info = new GameInfo(new String(), new BGGInfo(), Module.class, new Properties(), new String(), playerList, null, null);
        CommandResult result = new CommandResult();
        Module module = Mockito.mock(Module.class);

        VCCommand command = Mockito.mock(VCCommand.class);
        Mockito.when(command.getMinNumParameters()).thenReturn(2);
        Mockito.when(command.getMaxNumParameters()).thenReturn(2);

        IComponentPersister persister = Mockito.mock(IComponentPersister.class);
        BGGCommandProcessor sut = new BGGCommandProcessor(new BGGFormatter(), new CommandFormatter(), persister);

        // Test
        sut.processOneCommand(info, result, "player1", module, command, new String[]{"command"});

        // Assert
        Assert.assertFalse(result.isCompleted());
    }

    @Test
    public void commandWithTooManyArgumentsThrowsException() {
        // Setup
        ArrayList<String> playerList = new ArrayList<String>();
        playerList.add( "player1");
        GameInfo info = new GameInfo(new String(), new BGGInfo(), Module.class, new Properties(), new String(), playerList, null, null);
        CommandResult result = new CommandResult();
        Module module = Mockito.mock(Module.class);

        VCCommand command = Mockito.mock(VCCommand.class);
        Mockito.when(command.getMinNumParameters()).thenReturn(2);
        Mockito.when(command.getMaxNumParameters()).thenReturn(2);

        IComponentPersister persister = Mockito.mock(IComponentPersister.class);
        BGGCommandProcessor sut = new BGGCommandProcessor(new BGGFormatter(), new CommandFormatter(), persister);

        // Test
        sut.processOneCommand(info, result, "player1", module, command, new String[] { "command", "arg1", "arg2" });

        // Assert
        Assert.assertFalse(result.isCompleted());
    }

    @Test
    public void commandInWrongPhaseThrowsException() {
        // Setup
        ArrayList<String> playerList = new ArrayList<String>();
        playerList.add( "player1");
        GameInfo info = new GameInfo(new String(), new BGGInfo(), Module.class, new Properties(), new String(), playerList, null, new GamePhase() {
            @Override
            public int getState() {
                return 2;
            }

            @Override
            public String getStateName() {
                return "Actual phase";
            }
        });
        CommandResult result = new CommandResult();
        Module module = Mockito.mock(Module.class);

        VCCommand command = Mockito.mock(VCCommand.class);
        Mockito.when(command.getMinNumParameters()).thenReturn(1);
        Mockito.when(command.getMaxNumParameters()).thenReturn(1);
        GamePhase[] value = new GamePhase[] { new GamePhase() {
            @Override
            public int getState() {
                return 1;
            }

            @Override
            public String getStateName() {
                return "Desired phase";
            }
        }};
        Mockito.when(command.getValidPhases()).thenReturn(value);

        IComponentPersister persister = Mockito.mock(IComponentPersister.class);
        BGGCommandProcessor sut = new BGGCommandProcessor(new BGGFormatter(), new CommandFormatter(), persister);

        // Test
        sut.processOneCommand(info, result, "player1", module, command, new String[] { "command" });

        // Assert
        Assert.assertFalse(result.isCompleted());
    }

}
