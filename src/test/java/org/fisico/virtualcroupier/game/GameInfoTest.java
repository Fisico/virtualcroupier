package org.fisico.virtualcroupier.game;

import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Properties;

public class GameInfoTest {
    @Test
    public void testAddUserIncreaseTheNumberOfUsersWhenThereIsRoom() {
        GameInfo sut = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.SIGNUPS));
        sut.setMaxNumberOfPlayers(3);
        Assert.assertEquals(0,sut.getPlayers().size());
        sut.addPlayer("player");
        Assert.assertEquals(1,sut.getPlayers().size());
    }

    @Test
    public void testAddUserDoesNotIncreaseTheNumberOfUsersWhenThereIsNoRoom() {
        GameInfo sut = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.SIGNUPS));
        sut.setMaxNumberOfPlayers(0);
        Assert.assertEquals(0,sut.getPlayers().size());
        sut.addPlayer("player");
        Assert.assertEquals(0,sut.getPlayers().size());
    }

    @Test
    public void testIfMaxNumberPlayersNotSetPlayersCouldBeAdded() {
        GameInfo sut = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.SIGNUPS));
        Assert.assertEquals(0,sut.getPlayers().size());
        sut.addPlayer("player");
        Assert.assertEquals(1,sut.getPlayers().size());
    }
}
