package org.fisico.virtualcroupier.command;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CommandResultTest {

    @Test
    public void OneWithPublicAnotherWithPrivateResultsInPublicAndPrivate() {
        // Setup
        CommandResult sut = new CommandResult();
        CommandResult other = new CommandResult();
        sut.setPublicInfo("public");
        other.addPrivateInfo("player1", "private");

        // Test
        sut.combine(other);

        // Assert
        Assert.assertEquals("public", sut.getPublicInfo());
        List<MailInfo> result = sut.getPrivateInfo();
        Assert.assertEquals(1, result.size());
        MailInfo info = result.get(0);
        Assert.assertEquals("player1", info.getUser());
        Assert.assertEquals("private", info.getContent());
    }

    @Test
    public void TwoWithPublicResultsInPublicCombined() {
        // Setup
        CommandResult sut = new CommandResult();
        CommandResult other = new CommandResult();
        sut.setPublicInfo("public1");
        other.setPublicInfo("public2");

        // Test
        sut.combine(other);

        // Assert
        Assert.assertEquals(0 , sut.getPrivateInfo().size());
        Assert.assertNotEquals(-1, sut.getPublicInfo().indexOf("public1"));
        Assert.assertNotEquals(-1, sut.getPublicInfo().indexOf("public2"));
    }

    @Test
    public void PrivateFromDifferentUsersResultsInTwoEntriesInPrivate() {
        // Setup
        CommandResult sut = new CommandResult();
        CommandResult other = new CommandResult();
        sut.addPrivateInfo("player1", "private1");
        other.addPrivateInfo("player2", "private2");

        // Test
        sut.combine(other);

        // Assert
        Assert.assertNull(sut.getPublicInfo());
        List<MailInfo> result = sut.getPrivateInfo();
        Assert.assertEquals(2, result.size());
        MailInfo info = result.get(0);
        Assert.assertEquals("player1", info.getUser());
        Assert.assertEquals("private1", info.getContent());
        info = result.get(1);
        Assert.assertEquals("player2", info.getUser());
        Assert.assertEquals("private2", info.getContent());
    }

    @Test
    public void PrivateFromSameUserResultsInOneEntryCombined() {
        // Setup
        CommandResult sut = new CommandResult();
        CommandResult other = new CommandResult();
        sut.addPrivateInfo("player1", "private1");
        other.addPrivateInfo("player1", "private2");

        // Test
        sut.combine(other);

        // Assert
        Assert.assertNull(sut.getPublicInfo());
        List<MailInfo> result = sut.getPrivateInfo();
        Assert.assertEquals(1, result.size());
        MailInfo info = result.get(0);
        Assert.assertEquals("player1", info.getUser());
        Assert.assertNotEquals(-1, info.getContent().indexOf("private1"));
        Assert.assertNotEquals(-1, info.getContent().indexOf("private2"));
    }

}
