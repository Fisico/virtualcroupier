package org.fisico.virtualcroupier.command.base;

import org.fisico.virtualcroupier.command.ImpossibleCommandException;
import org.fisico.virtualcroupier.command.TableCommandHelper;
import org.fisico.virtualcroupier.components.Area;
import org.fisico.virtualcroupier.components.Deck;
import org.fisico.virtualcroupier.components.TableState;
import org.fisico.virtualcroupier.components.Visibility;
import org.fisico.virtualcroupier.game.BGGInfo;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Properties;

public class ShuffleCommandTest extends TableCommandHelper{

    @Test
    public void shuffleCommandShufflesTheRightDeck() throws ImpossibleCommandException {
		// Setup
		TableState state = new TableState();
        Area testArea = new Area("testArea", "player1", Visibility.PUBLIC_AREA);
        Deck deck1 = Mockito.spy(new Deck("deck1"));
        Deck deck2 = Mockito.spy(new Deck("deck2"));
        testArea.addDeck(deck1);
        testArea.addDeck(deck2);
        state.addArea(testArea);
		String[] command = { "shuffle", "deck2" };
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));
        Module module = Mockito.mock(Module.class);

        ShuffleCommand sut = new ShuffleCommand();
        setMockupOfState(sut, state);

        // Test
        sut.execute("player1", command);

        //Assert
        Mockito.verify(deck1, Mockito.times(0)).shuffle();
        Mockito.verify(deck2, Mockito.times(1)).shuffle();
    }

    @Test(expected = ImpossibleCommandException.class)
    public void canNotShuffleADeckThatDoesNotExist() throws ImpossibleCommandException {
        // Setup
        TableState state = new TableState();
        Area testArea = new Area("testArea", "player1", Visibility.PUBLIC_AREA);
        Deck deck1 = new Deck("deck1");
        testArea.addDeck(deck1);
        state.addArea(testArea);
        String[] command = { "shuffle", "deck2" };
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));
        Module module = Mockito.mock(Module.class);

        ShuffleCommand sut = new ShuffleCommand();
        setMockupOfState(sut, state);
        // Test
        sut.execute("player1", command);

        //Assert in signature
    }
}
