package org.fisico.virtualcroupier.command.base;

import org.fisico.virtualcroupier.command.ImpossibleCommandException;
import org.fisico.virtualcroupier.command.TableCommandHelper;
import org.fisico.virtualcroupier.components.*;
import org.fisico.virtualcroupier.game.BGGInfo;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class PickCommandTest extends TableCommandHelper {

    @Test
    public void PickFromDeckMovesCardToDeck() throws ImpossibleCommandException {
        // Setup
        TableState state = new TableState();
        Area testArea = new Area("testArea", "player1", Visibility.PUBLIC_AREA);
        List<Card> fromList = new ArrayList<Card>();
        for (int i = 0; i < 5; i++) {
            fromList.add(new Card("Moved_card_" + String.valueOf(i)));
        }
        Deck from = new Deck("from", fromList);
        Deck to = new Deck("to");
        testArea.addDeck(from);
        testArea.addDeck(to);
        state.addArea(testArea);
        String[] command = {"pick", "from", "to", "3"};
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));
        Module module = Mockito.mock(Module.class);

        PickCommand sut = new PickCommand();
        setMockupOfState(sut, state);

        // Test
        sut.execute("player1", command);

        // Assert
        assertEquals(4, from.size());
        assertEquals(1, to.size());
    }

    @Test
    public void PickFromDeckWithPositionMovesRightCard() throws ImpossibleCommandException {
        // Setup
        TableState state = new TableState();
        Area testArea = new Area("testArea", "player1", Visibility.PUBLIC_AREA);
        List<Card> fromList = new ArrayList<Card>();
        for (int i = 0; i < 5; i++) {
            fromList.add(new Card("Moved_card_" + String.valueOf(i)));
        }
        Deck from = new Deck("from", fromList);
        Deck to = new Deck("to");
        testArea.addDeck(from);
        testArea.addDeck(to);
        state.addArea(testArea);
        String[] command = {"pick", "from", "to", "3"};
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));
        Module module = Mockito.mock(Module.class);

        PickCommand sut = new PickCommand();
        setMockupOfState(sut, state);

        // Test
        sut.execute("player1", command);

        // Assert
        assertEquals(-1, to.toString().indexOf("Moved_card_0"));
        assertEquals(-1, to.toString().indexOf("Moved_card_1"));
        assertNotSame(-1, to.toString().indexOf("Moved_card_2"));
        assertEquals(-1, to.toString().indexOf("Moved_card_3"));
        assertEquals(-1, to.toString().indexOf("Moved_card_4"));
        assertNotSame(-1, from.toString().indexOf("Moved_card_0"));
        assertNotSame(-1, from.toString().indexOf("Moved_card_1"));
        assertEquals(-1, from.toString().indexOf("Moved_card_2"));
        assertNotSame(-1, from.toString().indexOf("Moved_card_3"));
        assertNotSame(-1, from.toString().indexOf("Moved_card_4"));
    }

    @Test
    public void PickFromDeckWithNameMovesRightCard() throws ImpossibleCommandException {
        // Setup
        TableState state = new TableState();
        Area testArea = new Area("testArea", "player1", Visibility.PUBLIC_AREA);
        List<Card> fromList = new ArrayList<Card>();
        for (int i = 0; i < 5; i++) {
            fromList.add(new Card("Moved_card_" + String.valueOf(i)));
        }
        Deck from = new Deck("from", fromList);
        Deck to = new Deck("to");
        testArea.addDeck(from);
        testArea.addDeck(to);
        state.addArea(testArea);
        String[] command = {"pick", "from", "to", "Moved_card_2"};
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));
        Module module = Mockito.mock(Module.class);

        PickCommand sut = new PickCommand();
        setMockupOfState(sut, state);

        // Test
        sut.execute("player1", command);

        // Assert
        assertEquals(-1, to.toString().indexOf("Moved_card_0"));
        assertEquals(-1, to.toString().indexOf("Moved_card_1"));
        assertNotSame(-1, to.toString().indexOf("Moved_card_2"));
        assertEquals(-1, to.toString().indexOf("Moved_card_3"));
        assertEquals(-1, to.toString().indexOf("Moved_card_4"));
        assertNotSame(-1, from.toString().indexOf("Moved_card_0"));
        assertNotSame(-1, from.toString().indexOf("Moved_card_1"));
        assertEquals(-1, from.toString().indexOf("Moved_card_2"));
        assertNotSame(-1, from.toString().indexOf("Moved_card_3"));
        assertNotSame(-1, from.toString().indexOf("Moved_card_4"));
    }
}
