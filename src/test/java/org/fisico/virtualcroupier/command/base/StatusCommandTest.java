package org.fisico.virtualcroupier.command.base;

import org.fisico.virtualcroupier.command.CommandResult;
import org.fisico.virtualcroupier.command.ImpossibleCommandException;
import org.fisico.virtualcroupier.command.TableCommandHelper;
import org.fisico.virtualcroupier.components.Area;
import org.fisico.virtualcroupier.components.TableState;
import org.fisico.virtualcroupier.components.Visibility;
import org.fisico.virtualcroupier.game.BGGInfo;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Properties;

public class StatusCommandTest extends TableCommandHelper{

    @Test
    public void statusShowsAllPublicAreas() throws ImpossibleCommandException {
        // Setup
        TableState state = new TableState();
        Area testArea1 = new Area("testArea1", "player1", Visibility.PUBLIC_AREA);
        testArea1.setVisibleWhenEmpty(true);
        Area testArea2 = new Area("testArea2", "player1", Visibility.PUBLIC_AREA);
        testArea2.setVisibleWhenEmpty(true);
        state.addArea(testArea1);
        state.addArea(testArea2);
        String[] command = { "status" };
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));
        Module module = Mockito.mock(Module.class);

        StatusCommand sut = new StatusCommand( );
        setMockupOfState(sut, state);
        // Test
        CommandResult result = sut.execute("player1", command);

        //Assert
        Assert.assertNotEquals(-1, result.getPublicInfo().indexOf("testArea1"));
        Assert.assertNotEquals(-1, result.getPublicInfo().indexOf("testArea2"));
    }

    @Test
    public void statusDoNotShowPrivateAreas() throws ImpossibleCommandException {
        // Setup
        TableState state = new TableState();
        Area testArea1 = new Area("testArea1", "player1", Visibility.PRIVATE_AREA);
        testArea1.setVisibleWhenEmpty(true);
        Area testArea2 = new Area("testArea2", "player1", Visibility.PRIVATE_AREA);
        testArea2.setVisibleWhenEmpty(true);
        state.addArea(testArea1);
        state.addArea(testArea2);
        String[] command = { "status" };
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));
        Module module = Mockito.mock(Module.class);

        StatusCommand sut = new StatusCommand( );
        setMockupOfState(sut, state);

        // Test
        CommandResult result = sut.execute("player1", command);

        //Assert
        boolean testArea1Shown = result.getPublicInfo() != null;
        if(testArea1Shown) testArea1Shown = result.getPublicInfo().indexOf("testArea1") != -1;
        boolean testArea2Shown = result.getPublicInfo() != null;
        if(testArea2Shown) testArea2Shown = result.getPublicInfo().indexOf("testArea2") != -1;

        Assert.assertFalse(testArea1Shown);
        Assert.assertFalse(testArea2Shown);

    }

    @Test
    public void statusOfAAreaShowsThatAreaAndNoOther() throws ImpossibleCommandException {
        // Setup
        TableState state = new TableState();
        Area testArea1 = new Area("testArea1", "player1", Visibility.PUBLIC_AREA);
        testArea1.setVisibleWhenEmpty(true);
        Area testArea2 = new Area("testArea2", "player1", Visibility.PUBLIC_AREA);
        testArea2.setVisibleWhenEmpty(true);
        state.addArea(testArea1);
        state.addArea(testArea2);
        String[] command = { "status", "testArea2" };
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));
        Module module = Mockito.mock(Module.class);

        StatusCommand sut = new StatusCommand( );
        setMockupOfState(sut, state);

        // Test
        CommandResult result = sut.execute("player1", command);

        //Assert
        Assert.assertEquals(-1, result.getPublicInfo().indexOf("testArea1"));
        Assert.assertNotEquals(-1, result.getPublicInfo().indexOf("testArea2"));

    }

}
