package org.fisico.virtualcroupier.command.base;

import org.fisico.geekbot.transform.BGGFormatter;
import org.fisico.virtualcroupier.command.*;
import org.fisico.virtualcroupier.game.BGGInfo;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.GamePhase;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class HelpCommandTest {

    @Test
    public void helpOfCommandIsShown() throws ImpossibleCommandException {
        //Setup
        Module module = Mockito.mock(Module.class);

        VCCommand command = new CommandImpl() {
            @Override
            public int getMinNumParameters() {
                return 1;
            }

            @Override
            public int getMaxNumParameters() {
                return 1;
            }

            @Override
            public GamePhase[] getValidPhases() {
                return new GamePhase[0];
            }

            @Override
                public String getName() {
                    return "testCommandName";
                }

                @Override
                public String getShortDescription() {
                    return "testCommandDescription";
                }

                @Override
                public String getHelp() {
                    return "testCommandMessage";
                }

                @Override
                public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
                    return new CommandResult();
                }
            };
        HashMap<String,VCCommand> map = new HashMap<>();
        Map<String,TableCommand> mapT= new HashMap<>();
        map.put("testCommandName", command);
        mapT.put("testCommandName", (TableCommand) command);
        Mockito.when(module.getAllCommands()).thenReturn(map);
        Mockito.when(module.getCommand("testCommandName")).thenReturn(command);
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));

        HelpCommand sut = new HelpCommand();
        sut.setModule(module);
        sut.setFormatter(new BGGFormatter());

        // Test
        CommandResult result = sut.execute("player1", new String[]{"help"});

        // Assert
        Assert.assertNotEquals(-1, result.getPublicInfo().indexOf("testCommandName"));
        Assert.assertNotEquals(-1, result.getPublicInfo().indexOf("testCommandDescription"));
        Assert.assertNotEquals(-1, result.getPublicInfo().indexOf("testCommandMessage"));
    }

    @Test
    public void helpOfACommandShowsOnlyHelpOfThatCommand() throws ImpossibleCommandException {
        //Setup
        Module module = Mockito.mock(Module.class);
        VCCommand command1 = new CommandImpl() {
            @Override
            public int getMinNumParameters() {
                return 1;
            }

            @Override
            public int getMaxNumParameters() {
                return 1;
            }

            @Override
            public GamePhase[] getValidPhases() {
                return new GamePhase[0];
            }

            @Override
            public String getName() {
                return "testCommandName1";
            }

            @Override
            public String getShortDescription() {
                return "testCommandDescription1";
            }

            @Override
            public String getHelp() {
                return "testCommandMessage1";
            }

            @Override
            public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
                return new CommandResult();
            }
        };
        VCCommand command2 = new CommandImpl() {
            @Override
            public int getMinNumParameters() {
                return 1;
            }

            @Override
            public int getMaxNumParameters() {
                return 1;
            }

            @Override
            public GamePhase[] getValidPhases() {
                return new GamePhase[0];
            }

            @Override
            public String getName() {
                return "testCommandName2";
            }

            @Override
            public String getShortDescription() {
                return "testCommandDescription2";
            }

            @Override
            public String getHelp() {
                return "testCommandMessage2";
            }

            @Override
            public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
                return new CommandResult();
            }
        };

        HashMap<String,VCCommand> map = new HashMap<>();
        Map<String,TableCommand> mapT= new HashMap<>();
        map.put("testCommandName1", command1);
        map.put("testCommandName2", command2);
        mapT.put("testCommandName1", (TableCommand) command1);
        mapT.put("testCommandName2", (TableCommand) command2);
        Mockito.when(module.getAllCommands()).thenReturn(map);
        Mockito.when(module.getCommand("testCommandName1")).thenReturn(command1);
        Mockito.when(module.getCommand("testCommandName2")).thenReturn(command2);
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));

        HelpCommand sut = new HelpCommand();
        sut.setModule(module);
        sut.setFormatter(new BGGFormatter());

        // Test
        CommandResult result = sut.execute("player1", new String[]{"help", "testCommandName2"});

        // Assert

        // Assert
        String resText = result.getPublicInfo();
        Assert.assertFalse( resText.length() < 56);
        Assert.assertFalse( resText.length() > 72);
        Assert.assertNotEquals(-1, resText.indexOf("testCommandName2"));
        Assert.assertNotEquals(-1, resText.indexOf("testCommandDescription2"));
        Assert.assertNotEquals(-1, resText.indexOf("testCommandMessage2"));
        Assert.assertEquals(-1, resText.indexOf("testCommandName1"));
        Assert.assertEquals(-1, resText.indexOf("testCommandDescription1"));
        Assert.assertEquals(-1, resText.indexOf("testCommandMessage1"));
    }

    @Test(expected = ImpossibleCommandException.class)
    public void helpOfANotExistingCommandThrowsException() throws ImpossibleCommandException {
        //Setup
        Module module = Mockito.mock(Module.class);
        VCCommand command = new CommandImpl() {
            @Override
            public int getMinNumParameters() {
                return 1;
            }

            @Override
            public int getMaxNumParameters() {
                return 1;
            }

            @Override
            public GamePhase[] getValidPhases() {
                return new GamePhase[0];
            }

            @Override
            public String getName() {
                return "testCommandName";
            }

            @Override
            public String getShortDescription() {
                return "testCommandDescription";
            }

            @Override
            public String getHelp() {
                return "testCommandMessage";
            }

            @Override
            public CommandResult execute(String user, String[] commandParts) throws ImpossibleCommandException {
                return new CommandResult();
            }
    };
        HashMap<String,VCCommand> map = new HashMap<>();
        Map<String,TableCommand> mapT= new HashMap<>();
        map.put("testCommandName", command);
        mapT.put("testCommandName", (TableCommand) command);
        Mockito.when(module.getAllCommands()).thenReturn(map);
        Mockito.when(module.getCommand("testCommandName")).thenReturn(command);
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.STARTED));

        HelpCommand sut = new HelpCommand();
        sut.setModule(module);
        sut.setFormatter(new BGGFormatter());

        // Test
        CommandResult result = sut.execute("player1", new String[]{"help", "nonExisting"});
    }

}
