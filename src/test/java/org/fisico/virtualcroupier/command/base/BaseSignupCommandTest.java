package org.fisico.virtualcroupier.command.base;

import org.fisico.virtualcroupier.command.ImpossibleCommandException;
import org.fisico.virtualcroupier.game.BGGInfo;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.base.BaseGamePhase;
import org.fisico.virtualcroupier.modules.Module;
import org.junit.Test;
import org.junit.Assert;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Properties;

public class BaseSignupCommandTest {

	@Test
	public void afterSignupTheUserIsAPlayer() throws ImpossibleCommandException {
		// Setup
		String[] command = { "signup" };
		GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.SIGNUPS));
		Module module = Mockito.mock(Module.class);

		BaseSignupCommand sut = new BaseSignupCommand();
		sut.setGameInfo(info);

		// Test
		sut.execute("player1", command);

		// Asserts
		Assert.assertTrue(info.getPlayers().contains("player1"));
	}

	@Test(expected = ImpossibleCommandException.class)
	public void theSamePlayerCanNotSignupTwoTimes() throws ImpossibleCommandException {

        // Setup
        String[] command = { "signup" };
        GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.SIGNUPS));
        Module module = Mockito.mock(Module.class);

		BaseSignupCommand sut = new BaseSignupCommand();
		sut.setGameInfo(info);
        try {
            sut.execute("player1", command);
        } catch(ImpossibleCommandException ex) {
            Assert.fail();
        }

        // Test

        try {
            sut.execute("player1", command);
			Assert.fail();
		} catch(ImpossibleCommandException ex) {
            // Asserts
            Assert.assertEquals(1, info.getPlayers().size());
            throw ex;
		}
	}

	@Test(expected = ImpossibleCommandException.class)
	public void ifThegameIsfullCanNotSignup() throws ImpossibleCommandException {
		// Setup
		GameInfo info = new GameInfo( new String(), new BGGInfo(), Module.class, new Properties(), new String(), new ArrayList<String>(), null, new BaseGamePhase(BaseGamePhase.SIGNUPS));
		info.setMaxNumberOfPlayers(1);
		String[] command = { "signup" };
        Module module = Mockito.mock(Module.class);

		BaseSignupCommand sut = new BaseSignupCommand();
        sut.setGameInfo(info);

		// Test
		try {
			sut.execute("player1", command);
		} catch (ImpossibleCommandException ex) {
			Assert.fail();
		}
        try {
            sut.execute("player1", command);
        } catch (ImpossibleCommandException ex) {
            // Assert
            Assert.assertEquals(1, info.getPlayers().size());
            throw ex;
        }
	}
}
