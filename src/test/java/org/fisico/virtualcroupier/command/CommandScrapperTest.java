/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fisico.virtualcroupier.command;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import junit.framework.TestCase;

/**
 *
 * @author afavieres
 */
public class CommandScrapperTest extends TestCase {

    @Test
    public void testRemoveQuotesFromText() {

        // Setup
        String test1 = "Let me briefly address a few of the things you mention in your post";
        String test2 = "<font color=#2121A4><div class='quote'><div class='quotetitle'><p><b>Starman54 wrote:</b></p></div><div class='quotebody'><i>I believe NSKN really need to get revamped rules</i></div></div></font><br/><br/>No good rulebook is a good manual.";
        String test3 = "<font color=#2121A4><div class='quote'><div class='quotetitle'><p><b>irondeav wrote:</b></p></div><div class='quotebody'><i>Actually, this is the first sentence of the <i>Mistfall</i> base game description here on the geek:<br/><br/><font color=#0B5C0D><div class='quote'><div class='quotebody'><i>Mistfall is a fully co-operative adventure game set in a fantasy world of mystery, eldritch powers and high adventure.</i></div></div></font><br/><br/>So, the new description kind of falls in line with the old one.</i></div></div></font><br/><br/>Was that description in the game";
        String test4 = "<font color=#2121A4><div class='quote'><div class='quotetitle'><p><b>Sphere wrote:</b></p></div><div class='quotebody'><i><font color=#0B5C0D><div class='quote'><div class='quotetitle'><p><b>Boofus wrote:</b></p></div><div class='quotebody'><i>Sorry, but I have to be THAT guy...</i></div></div></font><br/>I'd give you a thumb for mentioning clearly in the title that you're reviewing the rules only, but will refrain because that might lead someone to believe I endorse such reviews.<br/><br/>I expect <i><b>game</b></i> reviews here, per the folder title. Articles reviewing individual components may be useful, but I think they belong in General, not in Reviews.</i></div></div></font><br/><br/>Are you kidding me? I even put a disclaimer there:<br/><br/><font color=#2121A4><div class='quote'><div class='quotebody'><i>*This started out as a response to A warning to others..., but then I tought \"Hey, if that was a review, then so is mine!\". So, there you have it.</i></div></div></font><br/><br/>";
        String test5 = "<font color=#2121A4><div class='quote'><div class='quotetitle'><p><b>Sphere wrote:</b></p></div><div class='quotebody'><i><font color=#0B5C0D><div class='quote'><div class='quotetitle'><p><b>Boofus wrote:</b></p></div><div class='quotebody'><i>Sorry, but I have to be THAT guy...</i></div></div></font><br/>I'd give you a thumb for mentioning clearly in the title that you're reviewing the rules only, but will refrain because that might lead someone to believe I endorse such reviews.<br/><br/>I expect <i><b>game</b></i> reviews here, per the folder title. Articles reviewing individual components may be useful, but I think they belong in General, not in Reviews.</i></div></div></font><br/><br/>Are you kidding me? I even put a disclaimer there:<br/><br/><font color=#2121A4><div class='quote'><div class='quotebody'><i>*This started out as a response to A warning to others..., but then I tought \"Hey, if that was a review, then so is mine!\". So, there you have it.</i></div></div></font><br/><br/>So tell it to the moderators";

        // Test
        CommandScrapper scrapper = new CommandScrapper();
        String extracted1 = scrapper.cleanQuotes(test1);
        String extracted2 = scrapper.cleanQuotes(test2);
        String extracted3 = scrapper.cleanQuotes(test3);
        String extracted4 = scrapper.cleanQuotes(test4);
        String extracted5 = scrapper.cleanQuotes(test5);

        // Assert
        assertEquals("Let me briefly address a few of the things you mention in your post", extracted1);
        assertEquals("<br/><br/>No good rulebook is a good manual.", extracted2);
        assertEquals("<br/><br/>Was that description in the game", extracted3);
        assertEquals("<br/><br/>Are you kidding me? I even put a disclaimer there:<br/><br/><br/><br/>", extracted4);
        assertEquals("<br/><br/>Are you kidding me? I even put a disclaimer there:<br/><br/><br/><br/>So tell it to the moderators", extracted5);
    }

    @Test
    public void testExtractCommandText()
    {
        String test1 = "Something";
        String test2 = "<b>Something</b>";
        String test3 = "{Something}";
        String test4 = "<b>{command}</b>";
        String test5 = "<b>   {   command   }   </b>";
        String test6 = "<b>{command a b}</b>";
        String test7 = "<b>{command   a    b}</b>";
        String test8 = "Something so <b>{command}</b> and other things";
        String test9 = "<b>{command}</b><b>{command a b}</b>";

        String[] simpleCommand = new String[] {"command"};
        String[] parametrizedCommand = new String[] {"command", "a", "b"};
        List<String[]> empty = new ArrayList<String[]>();
        List<String[]> simple = new ArrayList<String[]>();
        simple.add(simpleCommand);
        List<String[]> parametrized = new ArrayList<String[]>();
        parametrized.add(parametrizedCommand);
        List<String[]> multiple = new ArrayList<String[]>();
        multiple.add(simpleCommand);
        multiple.add(parametrizedCommand);

        // Test
        CommandScrapper scrapper = new CommandScrapper();
        Assert.assertArrayEquals(empty.toArray(), scrapper.extractCommandText(test1).toArray());
        Assert.assertArrayEquals(empty.toArray(), scrapper.extractCommandText(test2).toArray());
        Assert.assertArrayEquals(empty.toArray(), scrapper.extractCommandText(test3).toArray());
        Assert.assertArrayEquals(simple.toArray(), scrapper.extractCommandText(test4).toArray());
        Assert.assertArrayEquals(simple.toArray(), scrapper.extractCommandText(test5).toArray());
        Assert.assertArrayEquals(parametrized.toArray(), scrapper.extractCommandText(test6).toArray());
        Assert.assertArrayEquals(parametrized.toArray(), scrapper.extractCommandText(test7).toArray());
        Assert.assertArrayEquals(simple.toArray(), scrapper.extractCommandText(test8).toArray());
        Assert.assertArrayEquals(multiple.toArray(), scrapper.extractCommandText(test9).toArray());
    }
}
