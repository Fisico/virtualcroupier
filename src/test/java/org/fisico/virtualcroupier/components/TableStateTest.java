package org.fisico.virtualcroupier.components;

import org.junit.Test;

import org.junit.Assert;

public class TableStateTest {

	@Test
	public void testAddAreaIncreaseTheNumberOfAreas() {
		TableState sut = new TableState();
		Assert.assertEquals(0,sut.getAreas().size());
		sut.addArea(new Area("testArea", "testPlayer"));
		Assert.assertEquals(1,sut.getAreas().size());
	}
	
    @Test
    public void testLocateDeck_AllRight() {
        Deck deck1 = new Deck("deck1");
        Deck deck2 = new Deck("deck2");
        Deck deck3 = new Deck("deck3");
        Deck deck4 = new Deck("deck4");
        Deck deck5 = new Deck("deck5");

        Area area12 = new Area("area12", "player1", Visibility.PRIVATE_AREA);
        Area area35 = new Area("area35", "player2", Visibility.PRIVATE_AREA);

        area12.addDeck(deck1);
        area12.addDeck(deck2);
        area35.addDeck(deck3);
        area35.addDeck(deck4);
        area35.addDeck(deck5);

        TableState sut = new TableState();
        sut.addArea(area12);
        sut.addArea(area35);
        Object located = sut.locateDeck("deck4", "player2");
        Assert.assertEquals(deck4, located);
    }
    
    @Test
    public void testLocateDeck_NoInArea_NotFound() {
    	Deck deck1 = new Deck("deck1");
        Deck deck2 = new Deck("deck2");
        Deck deck3 = new Deck("deck3");
        Deck deck4 = new Deck("deck4");
        Deck deck5 = new Deck("deck5");

        Area area12 = new Area("area12", "player1", Visibility.PRIVATE_AREA);
        Area area35 = new Area("area35", "player2", Visibility.PRIVATE_AREA);

        area12.addDeck(deck1);
        area12.addDeck(deck2);
        area35.addDeck(deck3);
        area35.addDeck(deck4);
        area35.addDeck(deck5);

        TableState sut = new TableState();
        sut.addArea(area12);
        sut.addArea(area35);
        Object located = sut.locateDeck("deck6", "player1");
        Assert.assertEquals(null, located);
    }
    
    @Test
    public void testIfTwoDecksWithSameNameReturnsFirstTheOneYouOwn() {
    	// Setup
    	TableState sut = new TableState();
    	addDeckWithSameName(sut, "area1", "player1");
    	addDeckWithSameName(sut, "area2", "player2");
    	addDeckWithSameName(sut, "area3", "player3");
    	addDeckWithSameName(sut, "area4", "player4");
    	addDeckWithSameName(sut, "area5", "player5");
    	addDeckWithSameName(sut, "area6", "player6");
    	Deck deck = addDeckWithSameName(sut, "area7", "player7");
    	addDeckWithSameName(sut, "area8", "player8");
        // Test
        Deck located = sut.locateDeck("deck", "player7");
        
        Assert.assertEquals(deck, located);
    }

	private Deck addDeckWithSameName(TableState sut, String areaName, String playerName) {
		Deck deck = new Deck("deck");
        Area area = new Area(areaName, playerName, Visibility.PUBLIC_AREA);
        area.addDeck(deck);
        sut.addArea(area);
        
        return deck;
	}
}
