package org.fisico.virtualcroupier.components;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 10/10/15.
 */
public class DeckTest {
	/**
	 * Test of peek method, of class Deck.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testPeek_emptyDeck_getsException() {
		Deck empty = new Deck("empty");
		empty.peek(new DeckOrderPosition(0));
	}

	@Test
	public void testPeek_notEmptyDeck_getsCard() {
		List<Card> cards = new ArrayList<Card>();
		for (int i = 0; i < 5; i++) {
			cards.add(new Card(String.valueOf(i)));
		}
		Deck notEmpty = new Deck("notEmpty", cards);
		assertEquals(cards.get(0).toString(), notEmpty.peek(new DeckOrderPosition(1)).toString());
		assertEquals(cards.get(2).toString(), notEmpty.peek(new DeckOrderPosition(3)).toString());
		assertEquals(cards.get(4).toString(), notEmpty.peek(new DeckOrderPosition(5)).toString());
	}

	@Test
	public void testPeek_notEmptyDeckBadPosition_getsException() {
		List<Card> cards = new ArrayList<Card>();
		for (int i = 0; i < 5; i++) {
			cards.add(new Card(String.valueOf(i)));
		}
		Deck notEmpty = new Deck("notEmpty", cards);
		try {
			notEmpty.peek(new DeckOrderPosition(6));
			Assert.fail();
		} catch(IllegalArgumentException ex) {
			;
		}
	}

	/**
	 * Test of draw method, of class Deck.
	 */
	@Test
	public void testDraw_int() {
		// Setup
		List<Card> cards = new ArrayList<Card>();
		for (int i = 0; i < 5; i++) {
			cards.add(new Card(String.valueOf(i)));
		}
		Deck test = new Deck("test", cards);
		// Test
		Card drawed = test.draw(new DeckOrderPosition(DeckOrderPosition.TOP.getPosition()));
		//Asserts
		assertEquals(cards.get(0).toString(), drawed.toString());
		assertEquals(4, test.size());
		// Test
		drawed = test.draw(new DeckOrderPosition(DeckOrderPosition.BOTTOM.getPosition()));
		//Asserts
		assertEquals(cards.get(4).toString(), drawed.toString());
		assertEquals(3, test.size());
		// Test
		drawed = test.draw(new DeckOrderPosition(2));
		//Asserts
		assertEquals(cards.get(2).toString(), drawed.toString());
		assertEquals(2, test.size());
	}

	/**
	 * Test of draw method, of class Deck.
	 */
	@Test
	public void testDraw_0args() {
		List<Card> cards = new ArrayList<Card>();
		for (int i = 0; i < 5; i++) {
			cards.add(new Card(String.valueOf(i)));
		}
		Deck test = new Deck("test", cards);
		Card drawed = test.draw();
		assertEquals(cards.get(0).toString(), drawed.toString());
	}

	/**
	 * Test of put method, of class Deck.
	 */
	@Test
	public void testPut() {
		List<Card> cards = new ArrayList<Card>();
		for (int i = 0; i < 5; i++) {
			cards.add(new Card(String.valueOf(i)));
		}
		Deck instance = new Deck("test");
		instance.put(new DeckOrderPosition(DeckOrderPosition.BOTTOM.getPosition()), cards.get(2));
		instance.put(new DeckOrderPosition(DeckOrderPosition.TOP.getPosition()), cards.get(0));
		instance.put(new DeckOrderPosition(2), cards.get(1));
		instance.put(new DeckOrderPosition(DeckOrderPosition.BOTTOM.getPosition()), cards.get(4));
		instance.put(new DeckOrderPosition(4), cards.get(3));

		for (int i = 0; i < 5; i++) {
			assertEquals(cards.get(i).toString(), instance.peek(new DeckOrderPosition(i + 1)).toString());
		}
	}

	/**
	 * Test of shuffle method, of class Deck.
	 */
	@Test
	public void testShuffleChangesTheOrder() {
		List<Card> cards1 = new ArrayList<Card>();
		List<Card> cards2 = new ArrayList<Card>(); 
		for (int i = 0; i < 200; i++) {
			cards1.add(new Card(String.valueOf(i)));
			cards2.add(new Card(String.valueOf(i)));
		}
		Deck original = new Deck("test1", cards1);
		Deck shuffled = new Deck("test2", cards2);
		shuffled.shuffle(1);
		boolean identical = true;
		for (int i = 1; i <= 200 && identical; i++) {
			String originalString = original.peek(new DeckOrderPosition(i)).toString();
			String shuffledString = shuffled.peek(new DeckOrderPosition(i)).toString();
			if (!originalString.equals(shuffledString)) {
				identical = false;
			}
		}
		assertFalse(identical);
	}
	
	@Test
	public void testShuffleDontChangeTheComposition() {
		List<Card> cards1 = new ArrayList<Card>();
		List<Card> cards2 = new ArrayList<Card>(); 
		for (int i = 0; i < 10; i++) {
			cards1.add(new Card(String.valueOf(i)));
			cards2.add(new Card(String.valueOf(i)));
		}
		Deck original = new Deck("test1", cards1);
		Deck shuffled = new Deck("test2", cards2);
		shuffled.shuffle(1);
		boolean identical = true;
		for (int i = 1; i <= 10 && identical; i++) {
			Card c = shuffled.peek(new DeckOrderPosition(i));
			if(! cardInDeck(c, original) ) {
				identical = false;
			}
		}
		for (int i = 1; i <= 10 && identical; i++) {
			Card c = original.peek(new DeckOrderPosition(i));
			if(! cardInDeck(c, shuffled) ) {
				identical = false;
			}
		}
		Assert.assertTrue(identical);
	}
	
	@Test
	public void testCutChangesTheOrder() {
		List<Card> cards1 = new ArrayList<Card>();
		List<Card> cards2 = new ArrayList<Card>(); 
		for (int i = 0; i < 200; i++) {
			cards1.add(new Card(String.valueOf(i)));
			cards2.add(new Card(String.valueOf(i)));
		}
		Deck original = new Deck("test1", cards1);
		Deck shuffled = new Deck("test2", cards2);
		shuffled.cut();
		boolean identical = true;
		for (int i = 1; i <= 200 && identical; i++) {
			String originalString = original.peek(new DeckOrderPosition(i)).toString();
			String shuffledString = shuffled.peek(new DeckOrderPosition(i)).toString();
			if (!originalString.equals(shuffledString)) {
				identical = false;
			}
		}
		assertFalse(identical);
	}
	
	@Test
	public void testCutDontChangeTheComposition() {
		List<Card> cards1 = new ArrayList<Card>();
		List<Card> cards2 = new ArrayList<Card>(); 
		for (int i = 0; i < 10; i++) {
			cards1.add(new Card(String.valueOf(i)));
			cards2.add(new Card(String.valueOf(i)));
		}
		Deck original = new Deck("test1", cards1);
		Deck shuffled = new Deck("test2", cards2);
		shuffled.cut();
		boolean identical = true;
		for (int i = 1; i <= shuffled.size() && identical; i++) {
			Card c = shuffled.peek(new DeckOrderPosition(i));
			if(! cardInDeck(c, original) ) {
				identical = false;
			}
		}
		for (int i = 1; i <= original.size() && identical; i++) {
			Card c = original.peek(new DeckOrderPosition(i));
			if(! cardInDeck(c, shuffled) ) {
				identical = false;
			}
		}
		Assert.assertTrue(identical);
	}
	
	private boolean cardInDeck(Card c, Deck d) {
		boolean found = false;
		for( int i = 1 ; i <= d.size() && !found; i++ ) {
			Card other = d.peek(new DeckOrderPosition(i));
			if( c.toString().equals(other.toString()) ) {
				found = true;
			}
		}
		
		return found;
	}

	/**
	 * Test of isEmpty method, of class Deck.
	 */
	@Test
	public void testIsEmpty() {
		List<Card> cards = new ArrayList<Card>();
		for (int i = 0; i < 5; i++) {
			cards.add(new Card(String.valueOf(i)));
		}
		Deck notEmpty = new Deck("notEmpty", cards);
		Deck empty = new Deck("empty");
		assertFalse(notEmpty.isEmpty());
		assertTrue(empty.isEmpty());
	}

	@Test
	public void testToString_WithCards_AllRight() {
		List<Card> cards = new ArrayList<Card>();
		for (int i = 1; i <= 5; i++) {
			cards.add(new Card(String.valueOf(i)));
		}
		Object test = new Deck("test", cards);
		String text = "[q=\"test\"]\n1 - 1\n2 - 2\n3 - 3\n4 - 4\n5 - 5\n[/q]\n";
		assertEquals(text, test.toString());
	}

	@Test
	public void testToString_WithoutCards_AllRight() {
		Object test = new Deck("test");
		String text = "[q=\"test\"]\n[/q]\n";
		assertEquals(text, test.toString());
	}
	
	@Test
	public void testLocateCard_AllRight() {
		// Setup
		List<Card> cards = new ArrayList<Card>();
		for (int i = 0; i < 5; i++) {
			cards.add(new Card("located_card_" + String.valueOf(i)));
		}
		Deck sut = new Deck("test", cards);
		// Test
		int position = sut.locate("located_card_2").value();
		// Asserts
		assertEquals(3, position);
	}

	@Test
	public void testLocateCard_NotFound_getsNotFound() {
		List<Card> cards = new ArrayList<Card>();
		for (int i = 0; i < 5; i++) {
			cards.add(new Card("located_card_" + String.valueOf(i)));
		}
		Deck sut = new Deck("test", cards);
		int position = sut.locate("located_card_8").value();
		assertEquals(Deck.NOT_FOUND, position);
	}

	@Test
	public void testLocateCardByNumber_AllRight() {
		// Setup
		List<Card> cards = new ArrayList<Card>();
		for (int i = 1; i <= 5; i++) {
			cards.add(new Card("located_card_" + String.valueOf(i)));
		}
		Deck sut = new Deck("test", cards);
		// Test
		int position = sut.locate(new CanonicalPosition(2)).value();
		// Asserts
		assertEquals(2, position);
	}

	@Test
	public void testLocateCardByNumber_NotFound_getsNotFound() {
		List<Card> cards = new ArrayList<Card>();
		for (int i = 1; i <= 5; i++) {
			cards.add(new Card("located_card_" + String.valueOf(i)));
		}
		Deck sut = new Deck("test", cards);
		int position = sut.locate(new CanonicalPosition(8)).value();
		assertEquals(Deck.NOT_FOUND, position);
	}

	
	@Test
	public void testToString_DeckEmptyAndNoVisibleWhenEmpty_DeckNotShown() {
		Deck sut = new Deck("test");
		sut.setVisibleWhenEmpty(false);
		assertEquals(-1, sut.toString().indexOf("test"));
	}

	@Test
	public void testRepresentationDoesNotChange() {
		List<Card> cards = new ArrayList<Card>();
		List<Card> otherCards = new ArrayList<Card>();
		for (int i = 0; i < 50; i++) {
			cards.add(new Card("card_" + String.valueOf(i)));
			otherCards.add(new Card("card_" + String.valueOf(i)));
		}	
		Deck one = new Deck("test", cards);
		one.shuffle();
		Deck other = new Deck("test", otherCards);
		other.shuffle();
		String oneString = one.toString();
		String otherString = other.toString();
		assertEquals(oneString , otherString );
	}

	@Test
	public void representationIsCaseInsensitiveAlphabeticalOrder() {
        // Setup
        List<Card> cards = new ArrayList<Card>();
        cards.add(new Card("Card-B"));
        cards.add(new Card("Card-1"));
        cards.add(new Card("Card-D"));
        cards.add(new Card("Card-A"));
        cards.add(new Card("Card-c"));

        Deck sut = new Deck("test", cards);

        // Test
        String res = sut.toString();

        // Assert
        Assert.assertTrue( res.indexOf("Card-1") < res.indexOf("Card-A"));
        Assert.assertTrue( res.indexOf("Card-A") < res.indexOf("Card-B"));
        Assert.assertTrue( res.indexOf("Card-B") < res.indexOf("Card-c"));
        Assert.assertTrue( res.indexOf("Card-c") < res.indexOf("Card-D"));
    }
}
