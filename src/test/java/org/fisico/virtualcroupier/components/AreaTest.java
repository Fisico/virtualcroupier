package org.fisico.virtualcroupier.components;

import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 10/10/15.
 */
public class AreaTest {

    @Test
    public void testGroupDecks()
    {
        Area area = new Area("TestArea", "player1");
        List<Card> cards = new ArrayList<Card>();
        for (int i = 0; i < 5; i++) {
            cards.add(new Card(String.valueOf(i)));
        }
        Deck deck1 = new Deck("deck1", cards);
        List<Card> cards2 = new ArrayList<Card>();
        for (int i = 10; i < 15; i++) {
            cards2.add(new Card(String.valueOf(i)));
        }
        Deck deck2 = new Deck("deck2", cards2);
        area.addDeck( deck1 );
        area.addDeck( deck2 );
        String output = area.toString();
        Assert.assertNotSame(-1, output.indexOf("deck1"));
        Assert.assertNotSame(-1, output.indexOf("deck2"));
    }

    @Test
    public void testGroupAreas()
    {
        Area area = new Area("TestArea", "player1");
        Area area1 = new Area("Area1", "player1");
        Area area2 = new Area("Area2", "player1");
        List<Card> cards = new ArrayList<Card>();
        for (int i = 0; i < 5; i++) {
            cards.add(new Card(String.valueOf(i)));
        }
        Deck deck1 = new Deck("deck1", cards);
        List<Card> cards2 = new ArrayList<Card>();
        for (int i = 10; i < 15; i++) {
            cards2.add(new Card(String.valueOf(i)));
        }
        Deck deck2 = new Deck("deck2", cards2);
        area1.addDeck( deck1 );
        area2.addDeck( deck2 );
        area.addArea(area1);
        area.addArea(area2);
        String output = area.toString();
        Assert.assertNotSame(-1, output.indexOf("Area1"));
        Assert.assertNotSame(-1, output.indexOf("Area2"));
        Assert.assertNotSame(-1, output.indexOf("deck1"));
        Assert.assertNotSame(-1, output.indexOf("deck2"));
    }

    @Test
    public void testRemoveDeck()
    {
        Area area = new Area("TestArea", "player1");
        Area area1 = new Area("Area1", "player1");
        Area area2 = new Area("Area2", "player1");
        List<Card> cards = new ArrayList<Card>();
        for (int i = 0; i < 5; i++) {
            cards.add(new Card(String.valueOf(i)));
        }
        Deck deck1 = new Deck("deck1", cards);
        List<Card> cards2 = new ArrayList<Card>();
        for (int i = 10; i < 15; i++) {
            cards2.add(new Card(String.valueOf(i)));
        }
        Deck deck2 = new Deck("deck2", cards2);
        area1.addDeck( deck1 );
        area2.addDeck( deck2 );
        area.addArea(area1);
        area.addArea(area2);
        area.removeDeck("deck1");
        String output = area.toString();
        Assert.assertEquals(-1, output.indexOf("deck1"));
    }
    
    @Test
    public void testSelectDeckWhenIsThere()
    {
    	Area area = new Area("TestArea", "player1", Visibility.PRIVATE_AREA);
        Area area1 = new Area("Area1", "player1", Visibility.PRIVATE_AREA);
        Area area2 = new Area("Area2", "player1", Visibility.PRIVATE_AREA);
        List<Card> cards = new ArrayList<Card>();
        for (int i = 0; i < 5; i++) {
            cards.add(new Card(String.valueOf(i)));
        }
        Deck deck1 = new Deck("deck1", cards);
        List<Card> cards2 = new ArrayList<Card>();
        for (int i = 10; i < 15; i++) {
            cards2.add(new Card(String.valueOf(i)));
        }
        Deck deck2 = new Deck("deck2", cards2);
        area1.addDeck( deck1 );
        area2.addDeck( deck2 );
        area.addArea(area1);
        area.addArea(area2);
        Object retrieved = area.selectDeck("deck1", "player1");
        Assert.assertEquals(deck1, retrieved);
    }
    
    @Test
    public void testSelectDeckWhenIsNotThere()
    {
    	Area area = new Area("TestArea", "player1", Visibility.PRIVATE_AREA);
        Area area1 = new Area("Area1", "player1", Visibility.PRIVATE_AREA);
        Area area2 = new Area("Area2", "player1", Visibility.PRIVATE_AREA);
        List<Card> cards = new ArrayList<Card>();
        for (int i = 0; i < 5; i++) {
            cards.add(new Card(String.valueOf(i)));
        }
        Deck deck1 = new Deck("deck1", cards);
        List<Card> cards2 = new ArrayList<Card>();
        for (int i = 10; i < 15; i++) {
            cards2.add(new Card(String.valueOf(i)));
        }
        Deck deck2 = new Deck("deck2", cards2);
        area1.addDeck( deck1 );
        area2.addDeck( deck2 );
        area.addArea(area1);
        area.addArea(area2);
        Object retrieved = area.selectDeck("deck1", "player2");
        Assert.assertEquals(null, retrieved);
    }
    
    @Test
    public void testAddCard()
    {
    	Area area = new Area("TestArea", "player1");
    	area.addCard(new Card("testCard"));
    	Assert.assertNotSame(-1, area.toString().indexOf("testCard"));
    }
    
    @Test
    public void testRemoveCard()
    {
    	Area area = new Area("TestArea", "player1");
    	area.addCard(new Card("testCard"));
    	area.removeCard( "testCard");
    	Assert.assertEquals(-1, area.toString().indexOf("testCard"));
    }
    
    @Test
    public void testOwnAreaCouldBeSeen() {
    	Area area = new Area("TestArea", "player1", Visibility.PRIVATE_AREA);
    	area.addCard(new Card("testCard"));
    	String seen = area.seenAs("player1", Visibility.PRIVATE_AREA);
    	Assert.assertNotSame(-1, seen.indexOf("testCard"));
    }
    
    @Test
    public void testPublicAreaOfAnotherCouldBeSeen() {
    	Area area = new Area("TestArea", "player1", Visibility.PUBLIC_AREA);
    	area.addCard(new Card("testCard"));
    	String seen = area.seenAs("player2", Visibility.PUBLIC_AREA);
    	Assert.assertNotSame(-1, seen.indexOf("testCard"));
    }
    
    @Test
    public void testPrivateAreaOfAnotherCouldNotBeSeen() {
    	Area area = new Area("TestArea", "player1", Visibility.PRIVATE_AREA);
    	area.addCard(new Card("testCard"));
    	String seen = area.seenAs("player2", Visibility.PRIVATE_AREA);
    	Assert.assertEquals(-1, seen.indexOf("testCard"));
    }
    
    @Test
    public void testChangeVisibilityReallyChanges() {
    	Area area = new Area("TestArea", "player1", Visibility.PRIVATE_AREA);
    	area.addCard(new Card("testCard"));
    	area.changeVisibility(Visibility.PUBLIC_AREA);
    	String seen = area.seenAs("player2", Visibility.PUBLIC_AREA);
    	Assert.assertNotSame(-1, seen.indexOf("testCard"));
    }
    
    @Test
    public void testAreaWithVisibleWhenEmptyToFalseIsNotVisibleWhenEmpty() {
    	// Setup
    	Area sut = new Area("TestArea", "player1", Visibility.PUBLIC_AREA);
    	sut.setVisibleWhenEmpty(false);
    	// Test
    	String result = sut.seenAs("player1", Visibility.PUBLIC_AREA);
    	// Assert
    	Assert.assertSame(-1, result.indexOf("TestArea"));
    }
}