package org.fisico.virtualcroupier;

import org.bson.types.ObjectId;
import org.fisico.virtualcroupier.game.BGGInfo;
import org.fisico.virtualcroupier.game.GameInfo;
import org.fisico.virtualcroupier.game.db.IGamePersister;
import org.fisico.virtualcroupier.modules.BaseModule;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class GamesWatchDogTest {
    @Test
    @Ignore
    public void dueGamesAreUpdated() {
        // Setup
        IGamePersister persister = Mockito.mock(IGamePersister.class);
        List<GameInfo> dueGame = new ArrayList<>();
        dueGame.add(new GameInfo(new String(),new BGGInfo(), BaseModule.class, new Properties(), new String(), new ArrayList<String>(), null, null));
        Mockito.when( persister.retrieveDueTimeGames(Mockito.anyInt())).thenReturn(dueGame).thenReturn(new ArrayList<GameInfo>());
        GameUpdater updater = Mockito.mock(GameUpdater.class);
        GamesWatchDog sut = new GamesWatchDog(persister, updater);

        // Test
        sut.updateGames();

        // Assert
        Mockito.verify(updater, Mockito.atLeast(1)).update(Mockito.any());
    }
}
